/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 24 "../gotd/parse.y"

#include "got_compat.h"

#include <sys/time.h>
#include <sys/types.h>
#include <sys/queue.h>
#include <sys/stat.h>

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <event.h>
#include <imsg.h>
#include <limits.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include "got_error.h"
#include "got_object.h"
#include "got_path.h"
#include "got_reference.h"

#include "log.h"
#include "gotd.h"
#include "auth.h"
#include "listen.h"
#include "secrets.h"

TAILQ_HEAD(files, file)		 files = TAILQ_HEAD_INITIALIZER(files);
static struct file {
	TAILQ_ENTRY(file)	 entry;
	FILE			*stream;
	char			*name;
	int			 lineno;
	int			 errors;
} *file;
struct file	*newfile(const char *, int, int);
static void	 closefile(struct file *);
int		 check_file_secrecy(int, const char *);
int		 yyparse(void);
int		 yylex(void);
int		 yyerror(const char *, ...)
    __attribute__((__format__ (printf, 1, 2)))
    __attribute__((__nonnull__ (1)));
int		 kw_cmp(const void *, const void *);
int		 lookup(char *);
int		 lgetc(int);
int		 lungetc(int);
int		 findeol(void);
static char	*port_sprintf(int);

TAILQ_HEAD(symhead, sym)	 symhead = TAILQ_HEAD_INITIALIZER(symhead);
struct sym {
	TAILQ_ENTRY(sym)	 entry;
	int			 used;
	int			 persist;
	char			*nam;
	char			*val;
};

int	 symset(const char *, const char *, int);
char	*symget(const char *);

static int		 errors;

static struct gotd		*gotd;
static struct gotd_repo		*new_repo;
static int			 conf_limit_user_connections(const char *, int);
static struct gotd_repo		*conf_new_repo(const char *);
static void			 conf_new_access_rule(struct gotd_repo *,
				    enum gotd_access, int, char *);
static int			 conf_protect_ref_namespace(char **,
				    struct got_pathlist_head *, char *);
static int			 conf_protect_tag_namespace(struct gotd_repo *,
				    char *);
static int			 conf_protect_branch_namespace(
				    struct gotd_repo *, char *);
static int			 conf_protect_branch(struct gotd_repo *,
				    char *);
static int			 conf_notify_branch(struct gotd_repo *,
				    char *);
static int			 conf_notify_ref_namespace(struct gotd_repo *,
				    char *);
static int			 conf_notify_email(struct gotd_repo *,
				    char *, char *, char *, char *, char *);
static int			 conf_notify_http(struct gotd_repo *,
				    char *, char *, char *, int);
static enum gotd_procid		 gotd_proc_id;

typedef struct {
	union {
		long long	 number;
		char		*string;
		struct timeval	 tv;
	} v;
	int lineno;
} YYSTYPE;


#line 176 "../gotd/parse.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    PATH = 258,                    /* PATH  */
    ERROR = 259,                   /* ERROR  */
    LISTEN = 260,                  /* LISTEN  */
    ON = 261,                      /* ON  */
    USER = 262,                    /* USER  */
    REPOSITORY = 263,              /* REPOSITORY  */
    PERMIT = 264,                  /* PERMIT  */
    DENY = 265,                    /* DENY  */
    RO = 266,                      /* RO  */
    RW = 267,                      /* RW  */
    CONNECTION = 268,              /* CONNECTION  */
    LIMIT = 269,                   /* LIMIT  */
    REQUEST = 270,                 /* REQUEST  */
    TIMEOUT = 271,                 /* TIMEOUT  */
    PROTECT = 272,                 /* PROTECT  */
    NAMESPACE = 273,               /* NAMESPACE  */
    BRANCH = 274,                  /* BRANCH  */
    TAG = 275,                     /* TAG  */
    REFERENCE = 276,               /* REFERENCE  */
    RELAY = 277,                   /* RELAY  */
    PORT = 278,                    /* PORT  */
    NOTIFY = 279,                  /* NOTIFY  */
    EMAIL = 280,                   /* EMAIL  */
    FROM = 281,                    /* FROM  */
    REPLY = 282,                   /* REPLY  */
    TO = 283,                      /* TO  */
    URL = 284,                     /* URL  */
    INSECURE = 285,                /* INSECURE  */
    HMAC = 286,                    /* HMAC  */
    AUTH = 287,                    /* AUTH  */
    STRING = 288,                  /* STRING  */
    NUMBER = 289                   /* NUMBER  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define PATH 258
#define ERROR 259
#define LISTEN 260
#define ON 261
#define USER 262
#define REPOSITORY 263
#define PERMIT 264
#define DENY 265
#define RO 266
#define RW 267
#define CONNECTION 268
#define LIMIT 269
#define REQUEST 270
#define TIMEOUT 271
#define PROTECT 272
#define NAMESPACE 273
#define BRANCH 274
#define TAG 275
#define REFERENCE 276
#define RELAY 277
#define PORT 278
#define NOTIFY 279
#define EMAIL 280
#define FROM 281
#define REPLY 282
#define TO 283
#define URL 284
#define INSECURE 285
#define HMAC 286
#define AUTH 287
#define STRING 288
#define NUMBER 289

/* Value type.  */


extern YYSTYPE yylval;


int yyparse (void);



/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_PATH = 3,                       /* PATH  */
  YYSYMBOL_ERROR = 4,                      /* ERROR  */
  YYSYMBOL_LISTEN = 5,                     /* LISTEN  */
  YYSYMBOL_ON = 6,                         /* ON  */
  YYSYMBOL_USER = 7,                       /* USER  */
  YYSYMBOL_REPOSITORY = 8,                 /* REPOSITORY  */
  YYSYMBOL_PERMIT = 9,                     /* PERMIT  */
  YYSYMBOL_DENY = 10,                      /* DENY  */
  YYSYMBOL_RO = 11,                        /* RO  */
  YYSYMBOL_RW = 12,                        /* RW  */
  YYSYMBOL_CONNECTION = 13,                /* CONNECTION  */
  YYSYMBOL_LIMIT = 14,                     /* LIMIT  */
  YYSYMBOL_REQUEST = 15,                   /* REQUEST  */
  YYSYMBOL_TIMEOUT = 16,                   /* TIMEOUT  */
  YYSYMBOL_PROTECT = 17,                   /* PROTECT  */
  YYSYMBOL_NAMESPACE = 18,                 /* NAMESPACE  */
  YYSYMBOL_BRANCH = 19,                    /* BRANCH  */
  YYSYMBOL_TAG = 20,                       /* TAG  */
  YYSYMBOL_REFERENCE = 21,                 /* REFERENCE  */
  YYSYMBOL_RELAY = 22,                     /* RELAY  */
  YYSYMBOL_PORT = 23,                      /* PORT  */
  YYSYMBOL_NOTIFY = 24,                    /* NOTIFY  */
  YYSYMBOL_EMAIL = 25,                     /* EMAIL  */
  YYSYMBOL_FROM = 26,                      /* FROM  */
  YYSYMBOL_REPLY = 27,                     /* REPLY  */
  YYSYMBOL_TO = 28,                        /* TO  */
  YYSYMBOL_URL = 29,                       /* URL  */
  YYSYMBOL_INSECURE = 30,                  /* INSECURE  */
  YYSYMBOL_HMAC = 31,                      /* HMAC  */
  YYSYMBOL_AUTH = 32,                      /* AUTH  */
  YYSYMBOL_STRING = 33,                    /* STRING  */
  YYSYMBOL_NUMBER = 34,                    /* NUMBER  */
  YYSYMBOL_35_n_ = 35,                     /* '\n'  */
  YYSYMBOL_36_ = 36,                       /* '='  */
  YYSYMBOL_37_ = 37,                       /* '{'  */
  YYSYMBOL_38_ = 38,                       /* '}'  */
  YYSYMBOL_YYACCEPT = 39,                  /* $accept  */
  YYSYMBOL_grammar = 40,                   /* grammar  */
  YYSYMBOL_varset = 41,                    /* varset  */
  YYSYMBOL_numberstring = 42,              /* numberstring  */
  YYSYMBOL_timeout = 43,                   /* timeout  */
  YYSYMBOL_main = 44,                      /* main  */
  YYSYMBOL_connection = 45,                /* connection  */
  YYSYMBOL_conflags_l = 46,                /* conflags_l  */
  YYSYMBOL_conflags = 47,                  /* conflags  */
  YYSYMBOL_protect = 48,                   /* protect  */
  YYSYMBOL_protectflags_l = 49,            /* protectflags_l  */
  YYSYMBOL_protectflags = 50,              /* protectflags  */
  YYSYMBOL_notify = 51,                    /* notify  */
  YYSYMBOL_notifyflags_l = 52,             /* notifyflags_l  */
  YYSYMBOL_notifyflags = 53,               /* notifyflags  */
  YYSYMBOL_repository = 54,                /* repository  */
  YYSYMBOL_55_1 = 55,                      /* $@1  */
  YYSYMBOL_repoopts1 = 56,                 /* repoopts1  */
  YYSYMBOL_repoopts2 = 57,                 /* repoopts2  */
  YYSYMBOL_nl = 58,                        /* nl  */
  YYSYMBOL_optnl = 59                      /* optnl  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   142

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  39
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  21
/* YYNRULES -- Number of rules.  */
#define YYNRULES  68
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  139

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   289


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      35,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    36,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    37,     2,    38,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   141,   141,   142,   143,   144,   145,   148,   166,   167,
     175,   183,   229,   246,   256,   259,   260,   262,   263,   266,
     275,   285,   286,   288,   289,   292,   302,   313,   325,   326,
     328,   329,   332,   343,   354,   366,   380,   394,   410,   424,
     440,   456,   474,   490,   508,   526,   546,   560,   576,   592,
     610,   622,   636,   650,   664,   680,   698,   698,   722,   764,
     771,   779,   786,   787,   790,   791,   794,   797,   798
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "PATH", "ERROR",
  "LISTEN", "ON", "USER", "REPOSITORY", "PERMIT", "DENY", "RO", "RW",
  "CONNECTION", "LIMIT", "REQUEST", "TIMEOUT", "PROTECT", "NAMESPACE",
  "BRANCH", "TAG", "REFERENCE", "RELAY", "PORT", "NOTIFY", "EMAIL", "FROM",
  "REPLY", "TO", "URL", "INSECURE", "HMAC", "AUTH", "STRING", "NUMBER",
  "'\\n'", "'='", "'{'", "'}'", "$accept", "grammar", "varset",
  "numberstring", "timeout", "main", "connection", "conflags_l",
  "conflags", "protect", "protectflags_l", "protectflags", "notify",
  "notifyflags_l", "notifyflags", "repository", "$@1", "repoopts1",
  "repoopts2", "nl", "optnl", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-31)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
     -31,     1,   -31,    -2,    -6,   -26,     6,   -23,   -31,   -12,
       3,   -31,    18,    29,   -31,   -31,   -31,   -31,    22,    59,
      41,   -31,    54,   -31,   -31,   -31,   -31,    49,    55,    16,
      41,    42,   -31,    41,    56,   -31,   -31,   -31,   -31,    45,
      41,    15,   -31,   -31,    42,    58,    53,    -6,    -4,    23,
     -31,   -31,    41,     2,   -31,   -31,    -6,    -6,   -31,   -16,
      71,    41,   -31,    60,    74,    35,    61,    41,   -31,   -31,
     -31,    62,   -31,   -31,    63,   -31,    65,    50,   -31,    66,
      67,    68,    40,    26,    41,   -31,   -31,   -31,    57,    41,
     -31,    75,     8,    69,    72,    70,    41,   -31,   -31,    50,
      73,    76,    79,   -31,    43,   -31,    26,   -31,    19,    81,
      77,    80,    82,   -31,    83,    84,    44,    91,    85,   -31,
      94,    86,   -31,   -31,    87,   -31,    46,    92,    98,   -31,
     -31,    89,    48,   100,   -31,   -31,    51,   -31,   -31
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     0,     3,     0,
       0,    14,     0,     0,     8,     9,    13,    56,     0,     0,
      68,    16,     0,     4,     5,     6,    12,     0,     0,     0,
      68,     0,     7,    68,     0,    11,    10,    19,    67,     0,
      68,     0,    20,    15,    18,     0,     0,     0,     0,     0,
      62,    63,    68,     0,    17,    58,     0,     0,    61,     0,
       0,    68,    22,     0,     0,     0,     0,    68,    29,    65,
      57,     0,    59,    60,     0,    27,     0,     0,    32,     0,
       0,     0,    50,     0,    68,    64,    26,    25,     0,    68,
      33,     0,    34,     0,     0,     0,    68,    66,    21,    24,
       0,     0,     0,    53,    51,    28,    31,    23,    35,    38,
       0,    52,     0,    30,     0,     0,     0,    36,     0,    54,
      39,     0,    42,    46,     0,    55,     0,    37,    40,    43,
      47,     0,     0,    41,    44,    48,     0,    45,    49
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -31,   -31,   -31,    11,   -31,   -31,   -31,    88,   118,   -31,
      27,    90,   -31,    21,    93,   -31,   -31,    78,   -31,   -31,
     -30
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,     1,     9,    16,    37,    10,    11,    39,    40,    50,
      88,    89,    51,    95,    96,    12,    27,    52,    53,    85,
      31
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      38,     2,    74,    41,    13,    45,     3,    17,     4,     5,
      44,    46,    47,    22,     6,    59,    60,    75,    45,    48,
      18,    19,    69,    23,    46,    47,    49,    14,    15,    28,
     101,    77,    48,    61,     7,   102,     8,    83,    24,    49,
      70,   114,    63,    20,    64,    63,   115,    64,    65,    35,
      36,    65,    66,    25,    97,    66,    18,    19,    58,    99,
      67,    80,    26,    81,    56,    57,   106,    72,    73,    59,
      60,    93,    94,   111,   112,    29,    30,   122,   123,   129,
     130,   134,   135,    43,   137,   138,    33,    32,    34,    76,
      42,    55,    79,    78,    82,    98,    86,    84,    87,    90,
      91,    92,   103,   100,   116,   104,   108,   110,   105,   109,
     117,   118,   121,   124,   131,   119,   120,   126,   125,   127,
     128,   132,   133,   136,    21,     0,   107,   113,     0,     0,
       0,    71,    54,     0,     0,     0,     0,     0,    62,     0,
       0,     0,    68
};

static const yytype_int8 yycheck[] =
{
      30,     0,    18,    33,     6,     3,     5,    33,     7,     8,
      40,     9,    10,    36,    13,    19,    20,    33,     3,    17,
      14,    15,    52,    35,     9,    10,    24,    33,    34,     7,
      22,    61,    17,    37,    33,    27,    35,    67,    35,    24,
      38,    22,    19,    37,    21,    19,    27,    21,    25,    33,
      34,    25,    29,    35,    84,    29,    14,    15,    47,    89,
      37,    26,    33,    28,    11,    12,    96,    56,    57,    19,
      20,    31,    32,    30,    31,    16,    35,    33,    34,    33,
      34,    33,    34,    38,    33,    34,    37,    33,    33,    18,
      34,    33,    18,    33,    33,    38,    33,    35,    33,    33,
      33,    33,    33,    28,    23,    33,    33,    28,    38,    33,
      33,    31,    28,    22,    22,    33,    33,    23,    33,    33,
      33,    23,    33,    23,     6,    -1,    99,   106,    -1,    -1,
      -1,    53,    44,    -1,    -1,    -1,    -1,    -1,    48,    -1,
      -1,    -1,    49
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    40,     0,     5,     7,     8,    13,    33,    35,    41,
      44,    45,    54,     6,    33,    34,    42,    33,    14,    15,
      37,    47,    36,    35,    35,    35,    33,    55,     7,    16,
      35,    59,    33,    37,    33,    33,    34,    43,    59,    46,
      47,    59,    34,    38,    59,     3,     9,    10,    17,    24,
      48,    51,    56,    57,    46,    33,    11,    12,    42,    19,
      20,    37,    50,    19,    21,    25,    29,    37,    53,    59,
      38,    56,    42,    42,    18,    33,    18,    59,    33,    18,
      26,    28,    33,    59,    35,    58,    33,    33,    49,    50,
      33,    33,    33,    31,    32,    52,    53,    59,    38,    59,
      28,    22,    27,    33,    33,    38,    59,    49,    33,    33,
      28,    30,    31,    52,    22,    27,    23,    33,    31,    33,
      33,    28,    33,    34,    22,    33,    23,    33,    33,    33,
      34,    22,    23,    33,    33,    34,    23,    33,    34
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    39,    40,    40,    40,    40,    40,    41,    42,    42,
      43,    43,    44,    44,    44,    45,    45,    46,    46,    47,
      47,    48,    48,    49,    49,    50,    50,    50,    51,    51,
      52,    52,    53,    53,    53,    53,    53,    53,    53,    53,
      53,    53,    53,    53,    53,    53,    53,    53,    53,    53,
      53,    53,    53,    53,    53,    53,    55,    54,    56,    56,
      56,    56,    56,    56,    57,    57,    58,    59,    59
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     3,     3,     3,     3,     1,     1,
       1,     1,     3,     2,     1,     5,     2,     3,     2,     3,
       4,     5,     2,     3,     2,     3,     3,     2,     5,     2,
       3,     2,     2,     3,     3,     5,     6,     8,     5,     7,
       8,    10,     7,     9,    10,    12,     7,     9,    10,    12,
       2,     4,     5,     4,     6,     7,     0,     7,     2,     3,
       3,     2,     1,     1,     3,     2,     2,     2,     0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 7: /* varset: STRING '=' STRING  */
#line 148 "../gotd/parse.y"
                                        {
			char *s = (yyvsp[-2].v.string);
			while (*s++) {
				if (isspace((unsigned char)*s)) {
					yyerror("macro name cannot contain "
					    "whitespace");
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			if (symset((yyvsp[-2].v.string), (yyvsp[0].v.string), 0) == -1)
				fatal("cannot store variable");
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1423 "../gotd/parse.c"
    break;

  case 9: /* numberstring: NUMBER  */
#line 167 "../gotd/parse.y"
                         {
			if (asprintf(&(yyval.v.string), "%lld", (long long)(yyvsp[0].v.number)) == -1) {
				yyerror("asprintf: %s", strerror(errno));
				YYERROR;
			}
		}
#line 1434 "../gotd/parse.c"
    break;

  case 10: /* timeout: NUMBER  */
#line 175 "../gotd/parse.y"
                         {
			if ((yyvsp[0].v.number) < 0) {
				yyerror("invalid timeout: %lld", (yyvsp[0].v.number));
				YYERROR;
			}
			(yyval.v.tv).tv_sec = (yyvsp[0].v.number);
			(yyval.v.tv).tv_usec = 0;
		}
#line 1447 "../gotd/parse.c"
    break;

  case 11: /* timeout: STRING  */
#line 183 "../gotd/parse.y"
                         {
			const char	*errstr;
			const char	*type = "seconds";
			size_t		 len;
			int		 mul = 1;

			if (*(yyvsp[0].v.string) == '\0') {
				yyerror("invalid number of seconds: %s", (yyvsp[0].v.string));
				free((yyvsp[0].v.string));
				YYERROR;
			}

			len = strlen((yyvsp[0].v.string));
			switch ((yyvsp[0].v.string)[len - 1]) {
			case 'S':
			case 's':
				(yyvsp[0].v.string)[len - 1] = '\0';
				break;
			case 'M':
			case 'm':
				type = "minutes";
				mul = 60;
				(yyvsp[0].v.string)[len - 1] = '\0';
				break;
			case 'H':
			case 'h':
				type = "hours";
				mul = 60 * 60;
				(yyvsp[0].v.string)[len - 1] = '\0';
				break;
			}

			(yyval.v.tv).tv_usec = 0;
			(yyval.v.tv).tv_sec = strtonum((yyvsp[0].v.string), 0, INT_MAX / mul, &errstr);
			if (errstr) {
				yyerror("number of %s is %s: %s", type,
				    errstr, (yyvsp[0].v.string));
				free((yyvsp[0].v.string));
				YYERROR;
			}

			(yyval.v.tv).tv_sec *= mul;
			free((yyvsp[0].v.string));
		}
#line 1496 "../gotd/parse.c"
    break;

  case 12: /* main: LISTEN ON STRING  */
#line 229 "../gotd/parse.y"
                                   {
			if (!got_path_is_absolute((yyvsp[0].v.string)))
				yyerror("bad unix socket path \"%s\": "
				    "must be an absolute path", (yyvsp[0].v.string));

			if (gotd_proc_id == PROC_LISTEN) {
				if (strlcpy(gotd->unix_socket_path, (yyvsp[0].v.string),
				    sizeof(gotd->unix_socket_path)) >=
				    sizeof(gotd->unix_socket_path)) {
					yyerror("%s: unix socket path too long",
					    __func__);
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1518 "../gotd/parse.c"
    break;

  case 13: /* main: USER numberstring  */
#line 246 "../gotd/parse.y"
                                    {
			if (strlcpy(gotd->user_name, (yyvsp[0].v.string),
			    sizeof(gotd->user_name)) >=
			    sizeof(gotd->user_name)) {
				yyerror("%s: user name too long", __func__);
				free((yyvsp[0].v.string));
				YYERROR;
			}
			free((yyvsp[0].v.string));
		}
#line 1533 "../gotd/parse.c"
    break;

  case 19: /* conflags: REQUEST TIMEOUT timeout  */
#line 266 "../gotd/parse.y"
                                                        {
			if ((yyvsp[0].v.tv).tv_sec <= 0) {
				yyerror("invalid timeout: %lld",
				    (long long)(yyvsp[0].v.tv).tv_sec);
				YYERROR;
			}
			memcpy(&gotd->request_timeout, &(yyvsp[0].v.tv),
			    sizeof(gotd->request_timeout));
		}
#line 1547 "../gotd/parse.c"
    break;

  case 20: /* conflags: LIMIT USER STRING NUMBER  */
#line 275 "../gotd/parse.y"
                                                {
			if (gotd_proc_id == PROC_LISTEN &&
			    conf_limit_user_connections((yyvsp[-1].v.string), (yyvsp[0].v.number)) == -1) {
				free((yyvsp[-1].v.string));
				YYERROR;
			}
			free((yyvsp[-1].v.string));
		}
#line 1560 "../gotd/parse.c"
    break;

  case 25: /* protectflags: TAG NAMESPACE STRING  */
#line 292 "../gotd/parse.y"
                                       {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_REPO_WRITE) {
				if (conf_protect_tag_namespace(new_repo, (yyvsp[0].v.string))) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1575 "../gotd/parse.c"
    break;

  case 26: /* protectflags: BRANCH NAMESPACE STRING  */
#line 302 "../gotd/parse.y"
                                          {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_REPO_WRITE) {
				if (conf_protect_branch_namespace(new_repo,
				    (yyvsp[0].v.string))) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1591 "../gotd/parse.c"
    break;

  case 27: /* protectflags: BRANCH STRING  */
#line 313 "../gotd/parse.y"
                                {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_REPO_WRITE) {
				if (conf_protect_branch(new_repo, (yyvsp[0].v.string))) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1606 "../gotd/parse.c"
    break;

  case 32: /* notifyflags: BRANCH STRING  */
#line 332 "../gotd/parse.y"
                                {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_branch(new_repo, (yyvsp[0].v.string))) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1622 "../gotd/parse.c"
    break;

  case 33: /* notifyflags: REFERENCE NAMESPACE STRING  */
#line 343 "../gotd/parse.y"
                                             {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_ref_namespace(new_repo, (yyvsp[0].v.string))) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1638 "../gotd/parse.c"
    break;

  case 34: /* notifyflags: EMAIL TO STRING  */
#line 354 "../gotd/parse.y"
                                  {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[0].v.string),
				    NULL, NULL, NULL)) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1655 "../gotd/parse.c"
    break;

  case 35: /* notifyflags: EMAIL FROM STRING TO STRING  */
#line 366 "../gotd/parse.y"
                                              {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-2].v.string), (yyvsp[0].v.string),
				    NULL, NULL, NULL)) {
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1674 "../gotd/parse.c"
    break;

  case 36: /* notifyflags: EMAIL TO STRING REPLY TO STRING  */
#line 380 "../gotd/parse.y"
                                                  {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-3].v.string),
				    (yyvsp[0].v.string), NULL, NULL)) {
					free((yyvsp[-3].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-3].v.string));
			free((yyvsp[0].v.string));
		}
#line 1693 "../gotd/parse.c"
    break;

  case 37: /* notifyflags: EMAIL FROM STRING TO STRING REPLY TO STRING  */
#line 394 "../gotd/parse.y"
                                                              {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-5].v.string), (yyvsp[-3].v.string),
				    (yyvsp[0].v.string), NULL, NULL)) {
					free((yyvsp[-5].v.string));
					free((yyvsp[-3].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-5].v.string));
			free((yyvsp[-3].v.string));
			free((yyvsp[0].v.string));
		}
#line 1714 "../gotd/parse.c"
    break;

  case 38: /* notifyflags: EMAIL TO STRING RELAY STRING  */
#line 410 "../gotd/parse.y"
                                               {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-2].v.string),
				    NULL, (yyvsp[0].v.string), NULL)) {
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1733 "../gotd/parse.c"
    break;

  case 39: /* notifyflags: EMAIL FROM STRING TO STRING RELAY STRING  */
#line 424 "../gotd/parse.y"
                                                           {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-4].v.string), (yyvsp[-2].v.string),
				    NULL, (yyvsp[0].v.string), NULL)) {
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1754 "../gotd/parse.c"
    break;

  case 40: /* notifyflags: EMAIL TO STRING REPLY TO STRING RELAY STRING  */
#line 440 "../gotd/parse.y"
                                                               {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-5].v.string),
				    (yyvsp[-2].v.string), (yyvsp[0].v.string), NULL)) {
					free((yyvsp[-5].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-5].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1775 "../gotd/parse.c"
    break;

  case 41: /* notifyflags: EMAIL FROM STRING TO STRING REPLY TO STRING RELAY STRING  */
#line 456 "../gotd/parse.y"
                                                                           {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-7].v.string), (yyvsp[-5].v.string),
				    (yyvsp[-2].v.string), (yyvsp[0].v.string), NULL)) {
					free((yyvsp[-7].v.string));
					free((yyvsp[-5].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-7].v.string));
			free((yyvsp[-5].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1798 "../gotd/parse.c"
    break;

  case 42: /* notifyflags: EMAIL TO STRING RELAY STRING PORT STRING  */
#line 474 "../gotd/parse.y"
                                                           {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-4].v.string),
				    NULL, (yyvsp[-2].v.string), (yyvsp[0].v.string))) {
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1819 "../gotd/parse.c"
    break;

  case 43: /* notifyflags: EMAIL FROM STRING TO STRING RELAY STRING PORT STRING  */
#line 490 "../gotd/parse.y"
                                                                       {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-6].v.string), (yyvsp[-4].v.string),
				    NULL, (yyvsp[-2].v.string), (yyvsp[0].v.string))) {
					free((yyvsp[-6].v.string));
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-6].v.string));
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1842 "../gotd/parse.c"
    break;

  case 44: /* notifyflags: EMAIL TO STRING REPLY TO STRING RELAY STRING PORT STRING  */
#line 508 "../gotd/parse.y"
                                                                           {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-7].v.string),
				    (yyvsp[-4].v.string), (yyvsp[-2].v.string), (yyvsp[0].v.string))) {
					free((yyvsp[-7].v.string));
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-7].v.string));
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1865 "../gotd/parse.c"
    break;

  case 45: /* notifyflags: EMAIL FROM STRING TO STRING REPLY TO STRING RELAY STRING PORT STRING  */
#line 526 "../gotd/parse.y"
                                                                                       {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-9].v.string), (yyvsp[-7].v.string),
				    (yyvsp[-4].v.string), (yyvsp[-2].v.string), (yyvsp[0].v.string))) {
					free((yyvsp[-9].v.string));
					free((yyvsp[-7].v.string));
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-9].v.string));
			free((yyvsp[-7].v.string));
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 1890 "../gotd/parse.c"
    break;

  case 46: /* notifyflags: EMAIL TO STRING RELAY STRING PORT NUMBER  */
#line 546 "../gotd/parse.y"
                                                           {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-4].v.string),
				    NULL, (yyvsp[-2].v.string), port_sprintf((yyvsp[0].v.number)))) {
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
		}
#line 1909 "../gotd/parse.c"
    break;

  case 47: /* notifyflags: EMAIL FROM STRING TO STRING RELAY STRING PORT NUMBER  */
#line 560 "../gotd/parse.y"
                                                                       {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-6].v.string), (yyvsp[-4].v.string),
				    NULL, (yyvsp[-2].v.string), port_sprintf((yyvsp[0].v.number)))) {
					free((yyvsp[-6].v.string));
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-6].v.string));
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
		}
#line 1930 "../gotd/parse.c"
    break;

  case 48: /* notifyflags: EMAIL TO STRING REPLY TO STRING RELAY STRING PORT NUMBER  */
#line 576 "../gotd/parse.y"
                                                                           {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, NULL, (yyvsp[-7].v.string),
				    (yyvsp[-4].v.string), (yyvsp[-2].v.string), port_sprintf((yyvsp[0].v.number)))) {
					free((yyvsp[-7].v.string));
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-7].v.string));
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
		}
#line 1951 "../gotd/parse.c"
    break;

  case 49: /* notifyflags: EMAIL FROM STRING TO STRING REPLY TO STRING RELAY STRING PORT NUMBER  */
#line 592 "../gotd/parse.y"
                                                                                       {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_email(new_repo, (yyvsp[-9].v.string), (yyvsp[-7].v.string),
				    (yyvsp[-4].v.string), (yyvsp[-2].v.string), port_sprintf((yyvsp[0].v.number)))) {
					free((yyvsp[-9].v.string));
					free((yyvsp[-7].v.string));
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-9].v.string));
			free((yyvsp[-7].v.string));
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
		}
#line 1974 "../gotd/parse.c"
    break;

  case 50: /* notifyflags: URL STRING  */
#line 610 "../gotd/parse.y"
                             {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_http(new_repo, (yyvsp[0].v.string), NULL,
				    NULL, 0)) {
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[0].v.string));
		}
#line 1991 "../gotd/parse.c"
    break;

  case 51: /* notifyflags: URL STRING AUTH STRING  */
#line 622 "../gotd/parse.y"
                                         {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_http(new_repo, (yyvsp[-2].v.string), (yyvsp[0].v.string), NULL,
				    0)) {
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 2010 "../gotd/parse.c"
    break;

  case 52: /* notifyflags: URL STRING AUTH STRING INSECURE  */
#line 636 "../gotd/parse.y"
                                                  {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_http(new_repo, (yyvsp[-3].v.string), (yyvsp[-1].v.string), NULL,
				    1)) {
					free((yyvsp[-3].v.string));
					free((yyvsp[-1].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-3].v.string));
			free((yyvsp[-1].v.string));
		}
#line 2029 "../gotd/parse.c"
    break;

  case 53: /* notifyflags: URL STRING HMAC STRING  */
#line 650 "../gotd/parse.y"
                                         {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_http(new_repo, (yyvsp[-2].v.string), NULL, (yyvsp[0].v.string),
				    0)) {
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 2048 "../gotd/parse.c"
    break;

  case 54: /* notifyflags: URL STRING AUTH STRING HMAC STRING  */
#line 664 "../gotd/parse.y"
                                                     {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_http(new_repo, (yyvsp[-4].v.string), (yyvsp[-2].v.string), (yyvsp[0].v.string),
				    0)) {
					free((yyvsp[-4].v.string));
					free((yyvsp[-2].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-4].v.string));
			free((yyvsp[-2].v.string));
			free((yyvsp[0].v.string));
		}
#line 2069 "../gotd/parse.c"
    break;

  case 55: /* notifyflags: URL STRING AUTH STRING INSECURE HMAC STRING  */
#line 680 "../gotd/parse.y"
                                                              {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (conf_notify_http(new_repo, (yyvsp[-5].v.string), (yyvsp[-3].v.string), (yyvsp[0].v.string),
				    1)) {
					free((yyvsp[-5].v.string));
					free((yyvsp[-3].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}
			free((yyvsp[-5].v.string));
			free((yyvsp[-3].v.string));
			free((yyvsp[0].v.string));
		}
#line 2090 "../gotd/parse.c"
    break;

  case 56: /* $@1: %empty  */
#line 698 "../gotd/parse.y"
                                    {
			struct gotd_repo *repo;

			TAILQ_FOREACH(repo, &gotd->repos, entry) {
				if (strcmp(repo->name, (yyvsp[0].v.string)) == 0) {
					yyerror("duplicate repository '%s'", (yyvsp[0].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
			}

			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_AUTH ||
			    gotd_proc_id == PROC_REPO_WRITE ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_GITWRAPPER |
			    gotd_proc_id == PROC_NOTIFY) {
				new_repo = conf_new_repo((yyvsp[0].v.string));
			}
			free((yyvsp[0].v.string));
		}
#line 2116 "../gotd/parse.c"
    break;

  case 57: /* repository: REPOSITORY STRING $@1 '{' optnl repoopts2 '}'  */
#line 718 "../gotd/parse.y"
                                          {
		}
#line 2123 "../gotd/parse.c"
    break;

  case 58: /* repoopts1: PATH STRING  */
#line 722 "../gotd/parse.y"
                              {
			if (gotd_proc_id == PROC_GOTD ||
			    gotd_proc_id == PROC_AUTH ||
			    gotd_proc_id == PROC_REPO_WRITE ||
			    gotd_proc_id == PROC_SESSION_WRITE ||
			    gotd_proc_id == PROC_GITWRAPPER ||
			    gotd_proc_id == PROC_NOTIFY) {
				if (!got_path_is_absolute((yyvsp[0].v.string))) {
					yyerror("%s: path %s is not absolute",
					    __func__, (yyvsp[0].v.string));
					free((yyvsp[0].v.string));
					YYERROR;
				}
				if (realpath((yyvsp[0].v.string), new_repo->path) == NULL) {
					/*
					 * To give admins a chance to create
					 * missing repositories at run-time
					 * we only warn about ENOENT here.
					 *
					 * And ignore 'permission denied' when
					 * running in gitwrapper. Users may be
					 * able to access this repository via
					 * gotd regardless.
					 */
					if (errno == ENOENT) {
						log_warn("%s", (yyvsp[0].v.string));
					} else if (errno != EACCES ||
					    gotd_proc_id != PROC_GITWRAPPER) {
						yyerror("realpath %s: %s", (yyvsp[0].v.string),
						    strerror(errno));
						free((yyvsp[0].v.string));
						YYERROR;
					}

					if (strlcpy(new_repo->path, (yyvsp[0].v.string),
					    sizeof(new_repo->path)) >=
					    sizeof(new_repo->path))
						yyerror("path too long");
				}
			}
			free((yyvsp[0].v.string));
		}
#line 2170 "../gotd/parse.c"
    break;

  case 59: /* repoopts1: PERMIT RO numberstring  */
#line 764 "../gotd/parse.y"
                                         {
			if (gotd_proc_id == PROC_AUTH) {
				conf_new_access_rule(new_repo,
				    GOTD_ACCESS_PERMITTED, GOTD_AUTH_READ, (yyvsp[0].v.string));
			} else
				free((yyvsp[0].v.string));
		}
#line 2182 "../gotd/parse.c"
    break;

  case 60: /* repoopts1: PERMIT RW numberstring  */
#line 771 "../gotd/parse.y"
                                         {
			if (gotd_proc_id == PROC_AUTH) {
				conf_new_access_rule(new_repo,
				    GOTD_ACCESS_PERMITTED,
				    GOTD_AUTH_READ | GOTD_AUTH_WRITE, (yyvsp[0].v.string));
			} else
				free((yyvsp[0].v.string));
		}
#line 2195 "../gotd/parse.c"
    break;

  case 61: /* repoopts1: DENY numberstring  */
#line 779 "../gotd/parse.y"
                                    {
			if (gotd_proc_id == PROC_AUTH) {
				conf_new_access_rule(new_repo,
				    GOTD_ACCESS_DENIED, 0, (yyvsp[0].v.string));
			} else
				free((yyvsp[0].v.string));
		}
#line 2207 "../gotd/parse.c"
    break;


#line 2211 "../gotd/parse.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 801 "../gotd/parse.y"


struct keywords {
	const char	*k_name;
	int		 k_val;
};

int
yyerror(const char *fmt, ...)
{
	va_list ap;
	char *msg;

	file->errors++;
	va_start(ap, fmt);
	if (vasprintf(&msg, fmt, ap) == -1)
		fatalx("yyerror vasprintf");
	va_end(ap);
	logit(LOG_CRIT, "%s:%d: %s", file->name, yylval.lineno, msg);
	free(msg);
	return (0);
}

int
kw_cmp(const void *k, const void *e)
{
	return (strcmp(k, ((const struct keywords *)e)->k_name));
}

int
lookup(char *s)
{
	/* This has to be sorted always. */
	static const struct keywords keywords[] = {
		{ "auth",			AUTH },
		{ "branch",			BRANCH },
		{ "connection",			CONNECTION },
		{ "deny",			DENY },
		{ "email",			EMAIL },
		{ "from",			FROM },
		{ "hmac",			HMAC },
		{ "insecure",			INSECURE },
		{ "limit",			LIMIT },
		{ "listen",			LISTEN },
		{ "namespace",			NAMESPACE },
		{ "notify",			NOTIFY },
		{ "on",				ON },
		{ "path",			PATH },
		{ "permit",			PERMIT },
		{ "port",			PORT },
		{ "protect",			PROTECT },
		{ "reference",			REFERENCE },
		{ "relay",			RELAY },
		{ "reply",			REPLY },
		{ "repository",			REPOSITORY },
		{ "request",			REQUEST },
		{ "ro",				RO },
		{ "rw",				RW },
		{ "tag",			TAG },
		{ "timeout",			TIMEOUT },
		{ "to",				TO },
		{ "url",			URL },
		{ "user",			USER },
	};
	const struct keywords *p;

	p = bsearch(s, keywords, sizeof(keywords)/sizeof(keywords[0]),
	    sizeof(keywords[0]), kw_cmp);

	if (p)
		return (p->k_val);
	else
		return (STRING);
}

#define MAXPUSHBACK	128

unsigned char *parsebuf;
int parseindex;
unsigned char pushback_buffer[MAXPUSHBACK];
int pushback_index = 0;

int
lgetc(int quotec)
{
	int c, next;

	if (parsebuf) {
		/* Read character from the parsebuffer instead of input. */
		if (parseindex >= 0) {
			c = parsebuf[parseindex++];
			if (c != '\0')
				return (c);
			parsebuf = NULL;
		} else
			parseindex++;
	}

	if (pushback_index)
		return (pushback_buffer[--pushback_index]);

	if (quotec) {
		c = getc(file->stream);
		if (c == EOF)
			yyerror("reached end of file while parsing "
			    "quoted string");
		return (c);
	}

	c = getc(file->stream);
	while (c == '\\') {
		next = getc(file->stream);
		if (next != '\n') {
			c = next;
			break;
		}
		yylval.lineno = file->lineno;
		file->lineno++;
		c = getc(file->stream);
	}

	return (c);
}

int
lungetc(int c)
{
	if (c == EOF)
		return (EOF);
	if (parsebuf) {
		parseindex--;
		if (parseindex >= 0)
			return (c);
	}
	if (pushback_index < MAXPUSHBACK-1)
		return (pushback_buffer[pushback_index++] = c);
	else
		return (EOF);
}

int
findeol(void)
{
	int c;

	parsebuf = NULL;

	/* Skip to either EOF or the first real EOL. */
	while (1) {
		if (pushback_index)
			c = pushback_buffer[--pushback_index];
		else
			c = lgetc(0);
		if (c == '\n') {
			file->lineno++;
			break;
		}
		if (c == EOF)
			break;
	}
	return (ERROR);
}

int
yylex(void)
{
	unsigned char buf[8096];
	unsigned char *p, *val;
	int quotec, next, c;
	int token;

top:
	p = buf;
	c = lgetc(0);
	while (c == ' ' || c == '\t')
		c = lgetc(0); /* nothing */

	yylval.lineno = file->lineno;
	if (c == '#') {
		c = lgetc(0);
		while (c != '\n' && c != EOF)
			c = lgetc(0); /* nothing */
	}
	if (c == '$' && parsebuf == NULL) {
		while (1) {
			c = lgetc(0);
			if (c == EOF)
				return (0);

			if (p + 1 >= buf + sizeof(buf) - 1) {
				yyerror("string too long");
				return (findeol());
			}
			if (isalnum(c) || c == '_') {
				*p++ = c;
				continue;
			}
			*p = '\0';
			lungetc(c);
			break;
		}
		val = symget(buf);
		if (val == NULL) {
			yyerror("macro '%s' not defined", buf);
			return (findeol());
		}
		parsebuf = val;
		parseindex = 0;
		goto top;
	}

	switch (c) {
	case '\'':
	case '"':
		quotec = c;
		while (1) {
			c = lgetc(quotec);
			if (c == EOF)
				return (0);
			if (c == '\n') {
				file->lineno++;
				continue;
			} else if (c == '\\') {
				next = lgetc(quotec);
				if (next == EOF)
					return (0);
				if (next == quotec || c == ' ' || c == '\t')
					c = next;
				else if (next == '\n') {
					file->lineno++;
					continue;
				} else
					lungetc(next);
			} else if (c == quotec) {
				*p = '\0';
				break;
			} else if (c == '\0') {
				yyerror("syntax error");
				return (findeol());
			}
			if (p + 1 >= buf + sizeof(buf) - 1) {
				yyerror("string too long");
				return (findeol());
			}
			*p++ = c;
		}
		yylval.v.string = strdup(buf);
		if (yylval.v.string == NULL)
			err(1, "yylex: strdup");
		return (STRING);
	}

#define allowed_to_end_number(x) \
	(isspace(x) || x == ')' || x ==',' || x == '/' || x == '}' || x == '=')

	if (c == '-' || isdigit(c)) {
		do {
			*p++ = c;
			if ((unsigned)(p-buf) >= sizeof(buf)) {
				yyerror("string too long");
				return (findeol());
			}
			c = lgetc(0);
		} while (c != EOF && isdigit(c));
		lungetc(c);
		if (p == buf + 1 && buf[0] == '-')
			goto nodigits;
		if (c == EOF || allowed_to_end_number(c)) {
			const char *errstr = NULL;

			*p = '\0';
			yylval.v.number = strtonum(buf, LLONG_MIN,
			    LLONG_MAX, &errstr);
			if (errstr) {
				yyerror("\"%s\" invalid number: %s",
				    buf, errstr);
				return (findeol());
			}
			return (NUMBER);
		} else {
nodigits:
			while (p > buf + 1)
				lungetc(*--p);
			c = *--p;
			if (c == '-')
				return (c);
		}
	}

#define allowed_in_string(x) \
	(isalnum(x) || (ispunct(x) && x != '(' && x != ')' && \
	x != '{' && x != '}' && \
	x != '!' && x != '=' && x != '#' && \
	x != ','))

	if (isalnum(c) || c == ':' || c == '_') {
		do {
			*p++ = c;
			if ((unsigned)(p-buf) >= sizeof(buf)) {
				yyerror("string too long");
				return (findeol());
			}
			c = lgetc(0);
		} while (c != EOF && (allowed_in_string(c)));
		lungetc(c);
		*p = '\0';
		token = lookup(buf);
		if (token == STRING) {
			yylval.v.string = strdup(buf);
			if (yylval.v.string == NULL)
				err(1, "yylex: strdup");
		}
		return (token);
	}
	if (c == '\n') {
		yylval.lineno = file->lineno;
		file->lineno++;
	}
	if (c == EOF)
		return (0);
	return (c);
}

int
check_file_secrecy(int fd, const char *fname)
{
	struct stat st;

	if (fstat(fd, &st)) {
		log_warn("cannot stat %s", fname);
		return (-1);
	}
	if (st.st_uid != 0 && st.st_uid != getuid()) {
		log_warnx("%s: owner not root or current user", fname);
		return (-1);
	}
	if (st.st_mode & (S_IWGRP | S_IXGRP | S_IRWXO)) {
		log_warnx("%s: group writable or world read/writable", fname);
		return (-1);
	}
	return (0);
}

struct file *
newfile(const char *name, int secret, int required)
{
	struct file *nfile;

	nfile = calloc(1, sizeof(struct file));
	if (nfile == NULL) {
		log_warn("calloc");
		return (NULL);
	}
	nfile->name = strdup(name);
	if (nfile->name == NULL) {
		log_warn("strdup");
		free(nfile);
		return (NULL);
	}
	nfile->stream = fopen(nfile->name, "r");
	if (nfile->stream == NULL) {
		if (required)
			log_warn("open %s", nfile->name);
		free(nfile->name);
		free(nfile);
		return (NULL);
	} else if (secret &&
	    check_file_secrecy(fileno(nfile->stream), nfile->name)) {
		fclose(nfile->stream);
		free(nfile->name);
		free(nfile);
		return (NULL);
	}
	nfile->lineno = 1;
	return (nfile);
}

static void
closefile(struct file *xfile)
{
	fclose(xfile->stream);
	free(xfile->name);
	free(xfile);
}

int
parse_config(const char *filename, enum gotd_procid proc_id,
    struct gotd_secrets *secrets, struct gotd *env)
{
	struct sym *sym, *next;
	struct gotd_repo *repo;
	int require_config_file = (proc_id != PROC_GITWRAPPER);

	memset(env, 0, sizeof(*env));

	gotd = env;
	gotd_proc_id = proc_id;
	gotd->secrets = secrets;
	TAILQ_INIT(&gotd->repos);

	/* Apply default values. */
	if (strlcpy(gotd->unix_socket_path, GOTD_UNIX_SOCKET,
	    sizeof(gotd->unix_socket_path)) >= sizeof(gotd->unix_socket_path)) {
		fprintf(stderr, "%s: unix socket path too long", __func__);
		return -1;
	}
	if (strlcpy(gotd->user_name, GOTD_USER,
	    sizeof(gotd->user_name)) >= sizeof(gotd->user_name)) {
		fprintf(stderr, "%s: user name too long", __func__);
		return -1;
	}

	gotd->request_timeout.tv_sec = GOTD_DEFAULT_REQUEST_TIMEOUT;
	gotd->request_timeout.tv_usec = 0;

	file = newfile(filename, 0, require_config_file);
	if (file == NULL)
		return require_config_file ? -1 : 0;

	yyparse();
	errors = file->errors;
	closefile(file);

	/* Free macros and check which have not been used. */
	TAILQ_FOREACH_SAFE(sym, &symhead, entry, next) {
		if ((gotd->verbosity > 1) && !sym->used)
			fprintf(stderr, "warning: macro '%s' not used\n",
			    sym->nam);
		if (!sym->persist) {
			free(sym->nam);
			free(sym->val);
			TAILQ_REMOVE(&symhead, sym, entry);
			free(sym);
		}
	}

	if (errors)
		return (-1);

	TAILQ_FOREACH(repo, &gotd->repos, entry) {
		if (repo->path[0] == '\0') {
			log_warnx("repository \"%s\": no path provided in "
			    "configuration file", repo->name);
			return (-1);
		}
	}

	if (proc_id == PROC_GOTD && TAILQ_EMPTY(&gotd->repos)) {
		log_warnx("no repository defined in configuration file");
		return (-1);
	}

	return (0);
}

static int
uid_connection_limit_cmp(const void *pa, const void *pb)
{
	const struct gotd_uid_connection_limit *a = pa, *b = pb;

	if (a->uid < b->uid)
		return -1;
	else if (a->uid > b->uid);
		return 1;

	return 0;
}

static int
conf_limit_user_connections(const char *user, int maximum)
{
	uid_t uid;
	struct gotd_uid_connection_limit *limit;
	size_t nlimits;

	if (maximum < 1) {
		yyerror("max connections cannot be smaller 1");
		return -1;
	}
	if (maximum > GOTD_MAXCLIENTS) {
		yyerror("max connections must be <= %d", GOTD_MAXCLIENTS);
		return -1;
	}

	if (gotd_parseuid(user, &uid) == -1) {
		yyerror("%s: no such user", user);
		return -1;
	}

	limit = gotd_find_uid_connection_limit(gotd->connection_limits,
	    gotd->nconnection_limits, uid);
	if (limit) {
		limit->max_connections = maximum;
		return 0;
	}

	limit = gotd->connection_limits;
	nlimits = gotd->nconnection_limits + 1;
	limit = reallocarray(limit, nlimits, sizeof(*limit));
	if (limit == NULL)
		fatal("reallocarray");

	limit[nlimits - 1].uid = uid;
	limit[nlimits - 1].max_connections = maximum;

	gotd->connection_limits = limit;
	gotd->nconnection_limits = nlimits;
	qsort(gotd->connection_limits, gotd->nconnection_limits,
	    sizeof(gotd->connection_limits[0]), uid_connection_limit_cmp);

	return 0;
}

static struct gotd_repo *
conf_new_repo(const char *name)
{
	struct gotd_repo *repo;

	if (name[0] == '\0') {
		fatalx("syntax error: empty repository name found in %s",
		    file->name);
	}

	if (strchr(name, '\n') != NULL)
		fatalx("repository names must not contain linefeeds: %s", name);

	repo = calloc(1, sizeof(*repo));
	if (repo == NULL)
		fatalx("%s: calloc", __func__);

	STAILQ_INIT(&repo->rules);
	TAILQ_INIT(&repo->protected_tag_namespaces);
	TAILQ_INIT(&repo->protected_branch_namespaces);
	TAILQ_INIT(&repo->protected_branches);
	TAILQ_INIT(&repo->protected_branches);
	TAILQ_INIT(&repo->notification_refs);
	TAILQ_INIT(&repo->notification_ref_namespaces);
	STAILQ_INIT(&repo->notification_targets);

	if (strlcpy(repo->name, name, sizeof(repo->name)) >=
	    sizeof(repo->name))
		fatalx("%s: strlcpy", __func__);

	TAILQ_INSERT_TAIL(&gotd->repos, repo, entry);
	gotd->nrepos++;

	return repo;
};

static void
conf_new_access_rule(struct gotd_repo *repo, enum gotd_access access,
    int authorization, char *identifier)
{
	struct gotd_access_rule *rule;

	rule = calloc(1, sizeof(*rule));
	if (rule == NULL)
		fatal("calloc");

	rule->access = access;
	rule->authorization = authorization;
	rule->identifier = identifier;

	STAILQ_INSERT_TAIL(&repo->rules, rule, entry);
}

static int
refname_is_valid(char *refname)
{
	if (strncmp(refname, "refs/", 5) != 0) {
		yyerror("reference name must begin with \"refs/\": %s",
		    refname);
		return 0;
	}

	if (!got_ref_name_is_valid(refname)) {
		yyerror("invalid reference name: %s", refname);
		return 0;
	}

	return 1;
}

static int
conf_protect_ref_namespace(char **new, struct got_pathlist_head *refs,
    char *namespace)
{
	const struct got_error *error;
	struct got_pathlist_entry *pe;
	char *s;

	*new = NULL;

	got_path_strip_trailing_slashes(namespace);
	if (!refname_is_valid(namespace))
		return -1;
	if (asprintf(&s, "%s/", namespace) == -1) {
		yyerror("asprintf: %s", strerror(errno));
		return -1;
	}

	error = got_pathlist_insert(&pe, refs, s, NULL);
	if (error || pe == NULL) {
		free(s);
		if (error)
			yyerror("got_pathlist_insert: %s", error->msg);
		else
			yyerror("duplicate protected namespace %s", namespace);
		return -1;
	}

	*new = s;
	return 0;
}

static int
conf_protect_tag_namespace(struct gotd_repo *repo, char *namespace)
{
	struct got_pathlist_entry *pe;
	char *new;

	if (conf_protect_ref_namespace(&new, &repo->protected_tag_namespaces,
	    namespace) == -1)
		return -1;

	TAILQ_FOREACH(pe, &repo->protected_branch_namespaces, entry) {
		if (strcmp(pe->path, new) == 0) {
			yyerror("duplicate protected namespace %s", namespace);
			return -1;
		}
	}

	return 0;
}

static int
conf_protect_branch_namespace(struct gotd_repo *repo, char *namespace)
{
	struct got_pathlist_entry *pe;
	char *new;

	if (conf_protect_ref_namespace(&new,
	    &repo->protected_branch_namespaces, namespace) == -1)
		return -1;

	TAILQ_FOREACH(pe, &repo->protected_tag_namespaces, entry) {
		if (strcmp(pe->path, new) == 0) {
			yyerror("duplicate protected namespace %s", namespace);
			return -1;
		}
	}

	return 0;
}

static int
conf_protect_branch(struct gotd_repo *repo, char *branchname)
{
	const struct got_error *error;
	struct got_pathlist_entry *new;
	char *refname;

	if (strncmp(branchname, "refs/heads/", 11) != 0) {
		if (asprintf(&refname, "refs/heads/%s", branchname) == -1) {
			yyerror("asprintf: %s", strerror(errno));
			return -1;
		}
	} else {
		refname = strdup(branchname);
		if (refname == NULL) {
			yyerror("strdup: %s", strerror(errno));
			return -1;
		}
	}

	if (!refname_is_valid(refname)) {
		free(refname);
		return -1;
	}

	error = got_pathlist_insert(&new, &repo->protected_branches,
	    refname, NULL);
	if (error || new == NULL) {
		free(refname);
		if (error)
			yyerror("got_pathlist_insert: %s", error->msg);
		else
			yyerror("duplicate protect branch %s", branchname);
		return -1;
	}

	return 0;
}

static int
conf_notify_branch(struct gotd_repo *repo, char *branchname)
{
	const struct got_error *error;
	struct got_pathlist_entry *pe;
	char *refname;

	if (strncmp(branchname, "refs/heads/", 11) != 0) {
		if (asprintf(&refname, "refs/heads/%s", branchname) == -1) {
			yyerror("asprintf: %s", strerror(errno));
			return -1;
		}
	} else {
		refname = strdup(branchname);
		if (refname == NULL) {
			yyerror("strdup: %s", strerror(errno));
			return -1;
		}
	}

	if (!refname_is_valid(refname)) {
		free(refname);
		return -1;
	}

	error = got_pathlist_insert(&pe, &repo->notification_refs,
	    refname, NULL);
	if (error) {
		free(refname);
		yyerror("got_pathlist_insert: %s", error->msg);
		return -1;
	}
	if (pe == NULL)
		free(refname);

	return 0;
}

static int
conf_notify_ref_namespace(struct gotd_repo *repo, char *namespace)
{
	const struct got_error *error;
	struct got_pathlist_entry *pe;
	char *s;

	got_path_strip_trailing_slashes(namespace);
	if (!refname_is_valid(namespace))
		return -1;

	if (asprintf(&s, "%s/", namespace) == -1) {
		yyerror("asprintf: %s", strerror(errno));
		return -1;
	}

	error = got_pathlist_insert(&pe, &repo->notification_ref_namespaces,
	    s, NULL);
	if (error) {
		free(s);
		yyerror("got_pathlist_insert: %s", error->msg);
		return -1;
	}
	if (pe == NULL)
		free(s);

	return 0;
}

static int
conf_notify_email(struct gotd_repo *repo, char *sender, char *recipient,
    char *responder, char *hostname, char *port)
{
	struct gotd_notification_target *target;

	STAILQ_FOREACH(target, &repo->notification_targets, entry) {
		if (target->type != GOTD_NOTIFICATION_VIA_EMAIL)
			continue;
		if (strcmp(target->conf.email.recipient, recipient) == 0) {
			yyerror("duplicate email notification for '%s' in "
			    "repository '%s'", recipient, repo->name);
			return -1;
		}
	}

	target = calloc(1, sizeof(*target));
	if (target == NULL)
		fatal("calloc");
	target->type = GOTD_NOTIFICATION_VIA_EMAIL;
	if (sender) {
		target->conf.email.sender = strdup(sender);
		if (target->conf.email.sender == NULL)
			fatal("strdup");
	}
	target->conf.email.recipient = strdup(recipient);
	if (target->conf.email.recipient == NULL)
		fatal("strdup");
	if (responder) {
		target->conf.email.responder = strdup(responder);
		if (target->conf.email.responder == NULL)
			fatal("strdup");
	}
	if (hostname) {
		target->conf.email.hostname = strdup(hostname);
		if (target->conf.email.hostname == NULL)
			fatal("strdup");
	}
	if (port) {
		target->conf.email.port = strdup(port);
		if (target->conf.email.port == NULL)
			fatal("strdup");
	}

	STAILQ_INSERT_TAIL(&repo->notification_targets, target, entry);
	return 0;
}

static int
conf_notify_http(struct gotd_repo *repo, char *url, char *auth, char *hmac,
    int insecure)
{
	const struct got_error *error;
	struct gotd_notification_target *target;
	char *proto, *hostname, *port, *path;
	int tls = 0, ret = 0;

	error = gotd_parse_url(&proto, &hostname, &port, &path, url);
	if (error) {
		yyerror("invalid HTTP notification URL '%s' in "
		    "repository '%s': %s", url, repo->name, error->msg);
		return -1;
	}

	tls = !strcmp(proto, "https");

	if (strcmp(proto, "http") != 0 && strcmp(proto, "https") != 0) {
		yyerror("invalid protocol '%s' in notification URL '%s' in "
		    "repository '%s", proto, url, repo->name);
		ret = -1;
		goto done;
	}

	if (port == NULL) {
		if (strcmp(proto, "http") == 0)
			port = strdup("80");
		if (strcmp(proto, "https") == 0)
			port = strdup("443");
		if (port == NULL) {
			error = got_error_from_errno("strdup");
			ret = -1;
			goto done;
		}
	}

	if (auth != NULL && gotd_proc_id == PROC_GOTD &&
	    (gotd->secrets == NULL || gotd_secrets_get(gotd->secrets,
	    GOTD_SECRET_AUTH, auth) == NULL)) {
		yyerror("no auth secret `%s' defined", auth);
		ret = -1;
		goto done;
	}

	if (hmac != NULL && gotd_proc_id == PROC_GOTD &&
	    (gotd->secrets == NULL && gotd_secrets_get(gotd->secrets,
	    GOTD_SECRET_HMAC, hmac) == NULL)) {
		yyerror("no hmac secret `%s' defined", hmac);
		ret = -1;
		goto done;
	}

	if (!insecure && strcmp(proto, "http") == 0 && auth) {
		yyerror("%s: HTTP notifications with basic authentication "
		    "over plaintext HTTP will leak credentials; add the "
		    "'insecure' config keyword if this is intentional", url);
		ret = -1;
		goto done;
	}

	STAILQ_FOREACH(target, &repo->notification_targets, entry) {
		if (target->type != GOTD_NOTIFICATION_VIA_HTTP)
			continue;
		if (target->conf.http.tls == tls &&
		    !strcmp(target->conf.http.hostname, hostname) &&
		    !strcmp(target->conf.http.port, port) &&
		    !strcmp(target->conf.http.path, path)) {
			yyerror("duplicate notification for URL '%s' in "
			    "repository '%s'", url, repo->name);
			ret = -1;
			goto done;
		}
	}

	target = calloc(1, sizeof(*target));
	if (target == NULL)
		fatal("calloc");
	target->type = GOTD_NOTIFICATION_VIA_HTTP;
	target->conf.http.tls = tls;
	target->conf.http.hostname = hostname;
	target->conf.http.port = port;
	target->conf.http.path = path;
	hostname = port = path = NULL;

	if (auth) {
		target->conf.http.auth = strdup(auth);
		if (target->conf.http.auth == NULL)
			fatal("strdup");
	}
	if (hmac) {
		target->conf.http.hmac = strdup(hmac);
		if (target->conf.http.hmac == NULL)
			fatal("strdup");
	}

	STAILQ_INSERT_TAIL(&repo->notification_targets, target, entry);
done:
	free(proto);
	free(hostname);
	free(port);
	free(path);
	return ret;
}

int
symset(const char *nam, const char *val, int persist)
{
	struct sym *sym;

	TAILQ_FOREACH(sym, &symhead, entry) {
		if (strcmp(nam, sym->nam) == 0)
			break;
	}

	if (sym != NULL) {
		if (sym->persist == 1)
			return (0);
		else {
			free(sym->nam);
			free(sym->val);
			TAILQ_REMOVE(&symhead, sym, entry);
			free(sym);
		}
	}
	sym = calloc(1, sizeof(*sym));
	if (sym == NULL)
		return (-1);

	sym->nam = strdup(nam);
	if (sym->nam == NULL) {
		free(sym);
		return (-1);
	}
	sym->val = strdup(val);
	if (sym->val == NULL) {
		free(sym->nam);
		free(sym);
		return (-1);
	}
	sym->used = 0;
	sym->persist = persist;
	TAILQ_INSERT_TAIL(&symhead, sym, entry);
	return (0);
}

char *
symget(const char *nam)
{
	struct sym *sym;

	TAILQ_FOREACH(sym, &symhead, entry) {
		if (strcmp(nam, sym->nam) == 0) {
			sym->used = 1;
			return (sym->val);
		}
	}
	return (NULL);
}

struct gotd_repo *
gotd_find_repo_by_name(const char *repo_name, struct gotd_repolist *repos)
{
	struct gotd_repo *repo;
	size_t namelen;

	TAILQ_FOREACH(repo, repos, entry) {
		namelen = strlen(repo->name);
		if (strncmp(repo->name, repo_name, namelen) != 0)
			continue;
		if (repo_name[namelen] == '\0' ||
		    strcmp(&repo_name[namelen], ".git") == 0)
			return repo;
	}

	return NULL;
}

struct gotd_repo *
gotd_find_repo_by_path(const char *repo_path, struct gotd *gotd)
{
	struct gotd_repo *repo;

	TAILQ_FOREACH(repo, &gotd->repos, entry) {
		if (strcmp(repo->path, repo_path) == 0)
			return repo;
	}

	return NULL;
}

struct gotd_uid_connection_limit *
gotd_find_uid_connection_limit(struct gotd_uid_connection_limit *limits,
    size_t nlimits, uid_t uid)
{
	/* This array is always sorted to allow for binary search. */
	int i, left = 0, right = nlimits - 1;

	while (left <= right) {
		i = ((left + right) / 2);
		if (limits[i].uid == uid)
			return &limits[i];
		if (limits[i].uid > uid)
			left = i + 1;
		else
			right = i - 1;
	}

	return NULL;
}

int
gotd_parseuid(const char *s, uid_t *uid)
{
	struct passwd *pw;
	const char *errstr;

	if ((pw = getpwnam(s)) != NULL) {
		*uid = pw->pw_uid;
		if (*uid == UID_MAX)
			return -1;
		return 0;
	}
	*uid = strtonum(s, 0, UID_MAX - 1, &errstr);
	if (errstr)
		return -1;
	return 0;
}

const struct got_error *
gotd_parse_url(char **proto, char **host, char **port,
    char **request_path, const char *url)
{
	const struct got_error *err = NULL;
	char *s, *p, *q;

	*proto = *host = *port = *request_path = NULL;

	p = strstr(url, "://");
	if (!p)
		return got_error(GOT_ERR_PARSE_URI);

	*proto = strndup(url, p - url);
	if (*proto == NULL) {
		err = got_error_from_errno("strndup");
		goto done;
	}
	s = p + 3;

	p = strstr(s, "/");
	if (p == NULL) {
		err = got_error(GOT_ERR_PARSE_URI);
		goto done;
	}

	q = memchr(s, ':', p - s);
	if (q) {
		*host = strndup(s, q - s);
		if (*host == NULL) {
			err = got_error_from_errno("strndup");
			goto done;
		}
		if ((*host)[0] == '\0') {
			err = got_error(GOT_ERR_PARSE_URI);
			goto done;
		}
		*port = strndup(q + 1, p - (q + 1));
		if (*port == NULL) {
			err = got_error_from_errno("strndup");
			goto done;
		}
		if ((*port)[0] == '\0') {
			err = got_error(GOT_ERR_PARSE_URI);
			goto done;
		}
	} else {
		*host = strndup(s, p - s);
		if (*host == NULL) {
			err = got_error_from_errno("strndup");
			goto done;
		}
		if ((*host)[0] == '\0') {
			err = got_error(GOT_ERR_PARSE_URI);
			goto done;
		}
	}

	while (p[0] == '/' && p[1] == '/')
		p++;
	*request_path = strdup(p);
	if (*request_path == NULL) {
		err = got_error_from_errno("strdup");
		goto done;
	}
	if ((*request_path)[0] == '\0') {
		err = got_error(GOT_ERR_PARSE_URI);
		goto done;
	}
done:
	if (err) {
		free(*proto);
		*proto = NULL;
		free(*host);
		*host = NULL;
		free(*port);
		*port = NULL;
		free(*request_path);
		*request_path = NULL;
	}
	return err;
}

static char *
port_sprintf(int p)
{
	static char portno[32];
	int n;

	n = snprintf(portno, sizeof(portno), "%lld", (long long)p);
	if (n < 0 || (size_t)n >= sizeof(portno))
		fatalx("port number too long: %lld", (long long)p);

	return portno;
}
