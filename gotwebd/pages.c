#line 2 "../gotwebd/pages.tmpl"
/*
* Copyright (c) 2022 Omar Polo <op@openbsd.org>
* Copyright (c) 2016, 2019, 2020-2022 Tracey Emery <tracey@traceyemery.net>
*
* Permission to use, copy, modify, and distribute this software for any
* purpose with or without fee is hereby granted, provided that the above
* copyright notice and this permission notice appear in all copies.
*
* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
* WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
* ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
* WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
* ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
* OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#line 19 "../gotwebd/pages.tmpl"
#include "got_compat.h"
#line 21 "../gotwebd/pages.tmpl"
#include <sys/types.h>
#include <sys/queue.h>
#include <sys/stat.h>
#line 25 "../gotwebd/pages.tmpl"
#include <ctype.h>
#include <event.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <imsg.h>
#line 33 "../gotwebd/pages.tmpl"
#include "got_error.h"
#include "got_object.h"
#include "got_reference.h"
#line 37 "../gotwebd/pages.tmpl"
#include "gotwebd.h"
#include "log.h"
#include "tmpl.h"
#line 41 "../gotwebd/pages.tmpl"
enum gotweb_ref_tm {
TM_DIFF,
TM_LONG,
};
#line 46 "../gotwebd/pages.tmpl"
static int breadcumbs(struct template *);
static int datetime(struct template *, time_t, int);
static int gotweb_render_blob_line(struct template *, const char *, size_t);
static int gotweb_render_tree_item(struct template *, struct got_tree_entry *);
static int blame_line(struct template *, const char *, struct blame_line *,
int, int);
#line 53 "../gotwebd/pages.tmpl"
static inline int gotweb_render_more(struct template *, int);
#line 55 "../gotwebd/pages.tmpl"
static inline int tree_listing(struct template *);
static inline int diff_line(struct template *, char *);
static inline int tag_item(struct template *, struct repo_tag *);
static inline int branch(struct template *, struct got_reflist_entry *);
static inline int rss_tag_item(struct template *, struct repo_tag *);
static inline int rss_author(struct template *, char *);
#line 62 "../gotwebd/pages.tmpl"
static inline char *
nextsep(char *s, char **t)
{
char *q;
#line 67 "../gotwebd/pages.tmpl"
while (*s == '/')
s++;
*t = s;
if (*s == '\0')
return NULL;
#line 73 "../gotwebd/pages.tmpl"
q = strchr(s, '/');
if (q == NULL)
q = strchr(s, '\0');
return q;
}
#line 81 "../gotwebd/pages.tmpl"
int
datetime(struct template *tp, time_t t, int fmt)
{
int tp_ret = 0;
#line 83 "../gotwebd/pages.tmpl"
struct tm tm;
char rfc3339[64];
char datebuf[64];
#line 87 "../gotwebd/pages.tmpl"
if (gmtime_r(&t, &tm) == NULL)
return -1;
#line 90 "../gotwebd/pages.tmpl"
if (strftime(rfc3339, sizeof(rfc3339), "%FT%TZ", &tm) == 0)
return -1;
#line 93 "../gotwebd/pages.tmpl"
if (fmt != TM_DIFF && asctime_r(&tm, datebuf) == NULL)
return -1;
#line 96 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<time datetime=\"", 16)) == -1) goto err;
#line 96 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rfc3339)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 97 "../gotwebd/pages.tmpl"
if (fmt == TM_DIFF) {
if ((tp_ret = gotweb_render_age(tp, t)) == -1) goto err;
} else {
if ((tp_ret = tp_htmlescape(tp, datebuf)) == -1)
goto err;
#line 100 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " UTC")) == -1)
goto err;
}
#line 103 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</time>", 7)) == -1) goto err;
err:
return tp_ret;
}
#line 105 "../gotwebd/pages.tmpl"
int
breadcumbs(struct template *tp)
{
int tp_ret = 0;
#line 107 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct querystring *qs = c->t->qs;
struct gotweb_url url;
const char *folder = qs->folder;
const char *action = "tree";
char *t, *s = NULL, *dir = NULL;
char ch;
#line 115 "../gotwebd/pages.tmpl"
memset(&url, 0, sizeof(url));
url.index_page = -1;
url.action = TREE;
url.path = qs->path;
url.commit = qs->commit;
#line 121 "../gotwebd/pages.tmpl"
if (qs->action != TREE && qs->action != BLOB) {
action = gotweb_action_name(qs->action);
url.action = qs->action;
}
#line 126 "../gotwebd/pages.tmpl"
if (folder && *folder != '\0') {
while (*folder == '/')
folder++;
dir = strdup(folder);
if (dir == NULL)
return (-1);
s = dir;
}
#line 135 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " / ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 136 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 136 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 136 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, action)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</a>", 4)) == -1) goto err;
#line 137 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " / ")) == -1)
goto err;
if (dir) {
while ((s = nextsep(s, &t)) != NULL) {
#line 141 "../gotwebd/pages.tmpl"
ch = *s;
*s = '\0';
url.folder = dir;
#line 146 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 146 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 147 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, t)) == -1)
goto err;
#line 149 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a>", 4)) == -1) goto err;
#line 149 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " / ")) == -1)
goto err;
#line 151 "../gotwebd/pages.tmpl"
*s = ch; 
}
}
#line 155 "../gotwebd/pages.tmpl"
if (qs->file) {
if ((tp_ret = tp_htmlescape(tp, qs->file)) == -1)
goto err;
}
#line 159 "../gotwebd/pages.tmpl"
err:
free(dir); 
return tp_ret;
}
#line 164 "../gotwebd/pages.tmpl"
int
gotweb_render_page(struct template *tp, int (*body)(struct template *))
{
int tp_ret = 0;
#line 166 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct querystring *qs = c->t->qs;
struct gotweb_url u_path;
const char *prfx = c->document_uri;
const char *css = srv->custom_css;
#line 173 "../gotwebd/pages.tmpl"
memset(&u_path, 0, sizeof(u_path));
u_path.index_page = -1;
u_path.action = SUMMARY;
#line 181 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<!doctype html><html><head><meta charset=\"utf-8\" /><title>", 58)) == -1) goto err;
#line 181 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, srv->site_name)) == -1)
goto err;
#line 185 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</title><meta name=\"viewport\" content=\"initial-scale=1.0\" /><meta name=\"msapplication-TileColor\" content=\"#da532c\" /><meta name=\"theme-color\" content=\"#ffffff\"/><link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"", 212)) == -1) goto err;
#line 185 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "apple-touch-icon.png\" /><link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"", 78)) == -1) goto err;
#line 186 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "favicon-32x32.png\" /><link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"", 75)) == -1) goto err;
#line 187 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "favicon-16x16.png\" /><link rel=\"manifest\" href=\"", 48)) == -1) goto err;
#line 188 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "site.webmanifest\"/><link rel=\"mask-icon\" href=\"", 47)) == -1) goto err;
#line 189 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "safari-pinned-tab.svg\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"", 70)) == -1) goto err;
#line 190 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
#line 190 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, css)) == -1)
goto err;
#line 195 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\" /></head><body><header id=\"header\"><div id=\"got_link\"><a href=\"", 65)) == -1) goto err;
#line 195 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, srv->logo_url)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "\" target=\"_blank\"><img src=\"", 28)) == -1) goto err;
#line 196 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, prfx)) == -1)
goto err;
#line 196 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, srv->logo)) == -1)
goto err;
#line 202 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\" /></a></div></header><nav id=\"site_path\"><div id=\"site_link\"><a href=\"?index_page=", 84)) == -1) goto err;
#line 202 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%d", qs->index_page) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 203 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, srv->site_link)) == -1)
goto err;
#line 205 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a>", 4)) == -1) goto err;
#line 205 "../gotwebd/pages.tmpl"
if (qs->path) {
u_path.path = qs->path; 
if ((tp_ret = tp_htmlescape(tp, " / ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 208 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &u_path)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 209 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, qs->path)) == -1)
goto err;
#line 211 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a>", 4)) == -1) goto err;
}
#line 213 "../gotwebd/pages.tmpl"
if (qs->action == SUMMARY ||qs->action == DIFF ||qs->action == TAG ||qs->action == TAGS) {
if ((tp_ret = tp_htmlescape(tp, " / ")) == -1)
goto err;
#line 214 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, gotweb_action_name(qs->action))) == -1)
goto err;
} else if (qs->action != INDEX) {
if ((tp_ret = breadcumbs(tp)) == -1) goto err;
}
#line 220 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div></nav><main class=\"action-", 32)) == -1) goto err;
#line 220 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, gotweb_action_name(qs->action))) == -1)
goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 221 "../gotwebd/pages.tmpl"
if ((tp_ret = body(tp)) == -1) goto err;
#line 225 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</main><footer id=\"site_owner_wrapper\"><p id=\"site_owner\">", 58)) == -1) goto err;
#line 225 "../gotwebd/pages.tmpl"
if (srv->show_site_owner) {
if ((tp_ret = tp_htmlescape(tp, srv->site_owner)) == -1)
goto err;
}
#line 232 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</p></footer></body></html>", 27)) == -1) goto err;
err:
return tp_ret;
}
#line 234 "../gotwebd/pages.tmpl"
int
gotweb_render_error(struct template *tp)
{
int tp_ret = 0;
#line 236 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
#line 240 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div id=\"err_content\">", 22)) == -1) goto err;
#line 240 "../gotwebd/pages.tmpl"
if (t->error) {
if ((tp_ret = tp_htmlescape(tp, t->error->msg)) == -1)
goto err;
} else {
#line 244 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "See daemon logs for details", 27)) == -1) goto err;
}
#line 246 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 248 "../gotwebd/pages.tmpl"
int
gotweb_render_repo_table_hdr(struct template *tp)
{
int tp_ret = 0;
#line 250 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
#line 257 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div id=\"index_header\"><div class=\"index_project\">Project</div>", 63)) == -1) goto err;
#line 257 "../gotwebd/pages.tmpl"
if (srv->show_repo_description) {
#line 261 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_project_description\">Description</div>", 56)) == -1) goto err;
}
if (srv->show_repo_owner) {
#line 266 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_project_owner\">Owner</div>", 44)) == -1) goto err;
}
if (srv->show_repo_age) {
#line 271 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_project_age\">Last Change</div>", 48)) == -1) goto err;
}
#line 273 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 275 "../gotwebd/pages.tmpl"
int
gotweb_render_repo_fragment(struct template *tp, struct repo_dir *repo_dir)
{
int tp_ret = 0;
#line 277 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct gotweb_url summary = {
.action = SUMMARY,
.index_page = -1,
.path = repo_dir->name,
}, briefs = {
.action = BRIEFS,
.index_page = -1,
.path = repo_dir->name,
}, commits = {
.action = COMMITS,
.index_page = -1,
.path = repo_dir->name,
}, tags = {
.action = TAGS,
.index_page = -1,
.path = repo_dir->name,
}, tree = {
.action = TREE,
.index_page = -1,
.path = repo_dir->name,
}, rss = {
.action = RSS,
.index_page = -1,
.path = repo_dir->name,
};
#line 307 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_wrapper\"><div class=\"index_project\"><a href=\"", 63)) == -1) goto err;
#line 307 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &summary)) == -1) goto err;
#line 307 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 307 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, repo_dir->name)) == -1)
goto err;
#line 309 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></div>", 10)) == -1) goto err;
#line 309 "../gotwebd/pages.tmpl"
if (srv->show_repo_description) {
#line 311 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_project_description\">", 39)) == -1) goto err;
#line 311 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, repo_dir->description)) == -1)
goto err;
#line 313 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
}
if (srv->show_repo_owner) {
#line 316 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_project_owner\">", 33)) == -1) goto err;
#line 316 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, repo_dir->owner)) == -1)
goto err;
#line 318 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
}
if (srv->show_repo_age) {
#line 321 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"index_project_age\">", 31)) == -1) goto err;
#line 321 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, repo_dir->age, TM_DIFF)) == -1) goto err;
#line 323 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
}
#line 326 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"navs_wrapper\"><div class=\"navs\"><a href=\"", 53)) == -1) goto err;
#line 326 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &summary)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">summary</a>", 13)) == -1) goto err;
#line 327 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 328 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &briefs)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">briefs</a>", 12)) == -1) goto err;
#line 329 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 330 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &commits)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">commits</a>", 13)) == -1) goto err;
#line 331 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 332 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &tags)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">tags</a>", 10)) == -1) goto err;
#line 333 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 334 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &tree)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">tree</a>", 10)) == -1) goto err;
#line 335 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 336 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &rss)) == -1) goto err;
#line 341 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">rss</a></div><hr /></div></div>", 33)) == -1) goto err;
err:
return tp_ret;
}
#line 343 "../gotwebd/pages.tmpl"
int
gotweb_render_briefs(struct template *tp)
{
int tp_ret = 0;
#line 345 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = c->t->qs;
struct repo_commit *rc;
struct repo_dir *repo_dir = t->repo_dir;
struct gotweb_url diff_url, patch_url, tree_url;
char *tmp, *body;
#line 353 "../gotwebd/pages.tmpl"
diff_url = (struct gotweb_url){
.action = DIFF,
.index_page = -1,
.path = repo_dir->name,
.headref = qs->headref,
};
patch_url = (struct gotweb_url){
.action = PATCH,
.index_page = -1,
.path = repo_dir->name,
.headref = qs->headref,
};
tree_url = (struct gotweb_url){
.action = TREE,
.index_page = -1,
.path = repo_dir->name,
.headref = qs->headref,
};
#line 376 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class='subtitle'><h2>Commit Briefs</h2></header><div id=\"briefs_content\">", 81)) == -1) goto err;
TAILQ_FOREACH(rc, &t->repo_commits, entry) {
#line 378 "../gotwebd/pages.tmpl"
diff_url.commit = rc->commit_id;
patch_url.commit = rc->commit_id;
tree_url.commit = rc->commit_id;
#line 382 "../gotwebd/pages.tmpl"
tmp = strchr(rc->committer, '<');
if (tmp)
*tmp = '\0';
#line 386 "../gotwebd/pages.tmpl"
body = strchr(rc->commit_msg, '\n');
if (body) {
*body++ = '\0';
while (*body == '\n')
body++;
}
#line 396 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class='brief'><p class='brief_meta'><span class='briefs_age'>", 66)) == -1) goto err;
#line 396 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rc->committer_time, TM_DIFF)) == -1) goto err;
#line 398 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span>", 7)) == -1) goto err;
#line 398 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 400 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<span class='briefs_id'>", 24)) == -1) goto err;
#line 400 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%.10s", rc->commit_id) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
#line 402 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span>", 7)) == -1) goto err;
#line 402 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 404 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<span class=\"briefs_author\">", 28)) == -1) goto err;
#line 404 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->committer)) == -1)
goto err;
#line 407 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span></p>", 11)) == -1) goto err;
#line 407 "../gotwebd/pages.tmpl"
if (body && *body != '\0') {
#line 410 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<details class=\"briefs_log\"><summary><a href=\"", 46)) == -1) goto err;
#line 410 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &diff_url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 411 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 413 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a>", 4)) == -1) goto err;
#line 413 "../gotwebd/pages.tmpl"
if (rc->refs_str) {
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 414 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<span class=\"refs_str\">(", 24)) == -1) goto err;
#line 414 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->refs_str)) == -1)
goto err;
if ((tp_ret = tp_write(tp, ")</span>", 8)) == -1) goto err;
}
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 418 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<span class=\"briefs_toggle\" aria-hidden=\"true\">", 47)) == -1) goto err;
#line 418 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ⋅⋅⋅ ")) == -1)
goto err;
#line 421 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span></summary>", 17)) == -1) goto err;
#line 421 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<p>", 3)) == -1) goto err;
#line 422 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, body)) == -1)
goto err;
#line 424 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</p></details>", 14)) == -1) goto err;
#line 424 "../gotwebd/pages.tmpl"
} else {
#line 426 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<p class=\"briefs_log\"><a href=\"", 31)) == -1) goto err;
#line 426 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &diff_url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 427 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 429 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a>", 4)) == -1) goto err;
#line 429 "../gotwebd/pages.tmpl"
if (rc->refs_str) {
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 430 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<span class=\"refs_str\">(", 24)) == -1) goto err;
#line 430 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->refs_str)) == -1)
goto err;
if ((tp_ret = tp_write(tp, ")</span>", 8)) == -1) goto err;
}
#line 433 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</p>", 4)) == -1) goto err;
}
#line 437 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"navs_wrapper\"><div class=\"navs\"><a href=\"", 59)) == -1) goto err;
#line 437 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &diff_url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">diff</a>", 10)) == -1) goto err;
#line 438 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 439 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &patch_url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">patch</a>", 11)) == -1) goto err;
#line 440 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 441 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &tree_url)) == -1) goto err;
#line 445 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">tree</a></div></div><hr />", 28)) == -1) goto err;
}
if ((tp_ret = gotweb_render_more(tp, BRIEFS)) == -1) goto err;
#line 448 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 450 "../gotwebd/pages.tmpl"
int
gotweb_render_more(struct template *tp, int action)
{
int tp_ret = 0;
#line 452 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct gotweb_url more = {
.action = action,
.index_page = -1,
.path = qs->path,
.commit = t->more_id,
.headref = qs->headref,
.file = qs->file,
};
#line 464 "../gotwebd/pages.tmpl"
if (action == TAGS)
more.commit = t->tags_more_id;
#line 467 "../gotwebd/pages.tmpl"
if (more.commit) {
#line 470 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div id=\"np_wrapper\"><div id=\"nav_more\"><a href=\"", 49)) == -1) goto err;
#line 470 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &more)) == -1) goto err;
#line 475 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">More&nbsp;&darr;</a></div></div>", 34)) == -1) goto err;
}
err:
return tp_ret;
}
#line 478 "../gotwebd/pages.tmpl"
int
gotweb_render_navs(struct template *tp)
{
int tp_ret = 0;
#line 480 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct gotweb_url prev, next;
int have_prev, have_next;
#line 484 "../gotwebd/pages.tmpl"
gotweb_index_navs(c, &prev, &have_prev, &next, &have_next);
#line 488 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div id=\"np_wrapper\"><div id=\"nav_prev\">", 40)) == -1) goto err;
#line 488 "../gotwebd/pages.tmpl"
if (have_prev) {
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 489 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &prev)) == -1) goto err;
#line 492 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Previous</a>", 14)) == -1) goto err;
}
#line 495 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div id=\"nav_next\">", 25)) == -1) goto err;
#line 495 "../gotwebd/pages.tmpl"
if (have_next) {
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 496 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &next)) == -1) goto err;
#line 499 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Next</a>", 10)) == -1) goto err;
}
#line 502 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div></div>", 12)) == -1) goto err;
err:
return tp_ret;
}
#line 504 "../gotwebd/pages.tmpl"
int
gotweb_render_commits(struct template *tp)
{
int tp_ret = 0;
#line 506 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
struct repo_commit *rc;
struct gotweb_url diff, patch, tree;
#line 512 "../gotwebd/pages.tmpl"
diff = (struct gotweb_url){
.action = DIFF,
.index_page = -1,
.path = repo_dir->name,
};
patch = (struct gotweb_url){
.action = PATCH,
.index_page = -1,
.path = repo_dir->name,
};
tree = (struct gotweb_url){
.action = TREE,
.index_page = -1,
.path = repo_dir->name,
};
#line 532 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class=\"subtitle\"><h2>Commits</h2></header><div class=\"commits_content\">", 79)) == -1) goto err;
TAILQ_FOREACH(rc, &t->repo_commits, entry) {
#line 534 "../gotwebd/pages.tmpl"
diff.commit = rc->commit_id;
patch.commit = rc->commit_id;
tree.commit = rc->commit_id;
#line 541 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"page_header_wrapper\"><dl><dt>Commit:</dt><dd><code class=\"commit-id\">", 81)) == -1) goto err;
#line 541 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_id)) == -1)
goto err;
#line 543 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</code></dd><dt>From:</dt><dd>", 30)) == -1) goto err;
#line 543 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->author)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
#line 544 "../gotwebd/pages.tmpl"
if (strcmp(rc->committer, rc->author) != 0) {
#line 546 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Via:</dt><dd>", 17)) == -1) goto err;
#line 546 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->committer)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
}
#line 550 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Date:</dt><dd>", 18)) == -1) goto err;
#line 550 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rc->committer_time, TM_LONG)) == -1) goto err;
#line 556 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd></dl></div><hr /><div class=\"commit\">", 42)) == -1) goto err;
#line 556 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 561 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"navs_wrapper\"><div class=\"navs\"><a href=\"", 59)) == -1) goto err;
#line 561 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &diff)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">diff</a>", 10)) == -1) goto err;
#line 562 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 563 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &patch)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">patch</a>", 11)) == -1) goto err;
#line 564 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 565 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &tree)) == -1) goto err;
#line 569 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">tree</a></div></div><hr />", 28)) == -1) goto err;
}
if ((tp_ret = gotweb_render_more(tp, COMMITS)) == -1) goto err;
#line 572 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 574 "../gotwebd/pages.tmpl"
int
gotweb_render_blob(struct template *tp)
{
int tp_ret = 0;
#line 576 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct got_blob_object *blob = t->blob;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
struct gotweb_url briefs_url, blame_url, raw_url;
#line 583 "../gotwebd/pages.tmpl"
memset(&briefs_url, 0, sizeof(briefs_url));
briefs_url.index_page = -1,
briefs_url.action = BRIEFS,
briefs_url.path = qs->path,
briefs_url.commit = qs->commit,
briefs_url.folder = qs->folder,
briefs_url.file = qs->file,
#line 591 "../gotwebd/pages.tmpl"
memcpy(&blame_url, &briefs_url, sizeof(blame_url));
blame_url.action = BLAME;
#line 594 "../gotwebd/pages.tmpl"
memcpy(&raw_url, &briefs_url, sizeof(raw_url));
raw_url.action = BLOBRAW;
#line 605 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class=\"subtitle\"><h2>Blob</h2></header><div id=\"blob_content\"><div class=\"page_header_wrapper\"><dl><dt>Date:</dt><dd>", 125)) == -1) goto err;
#line 605 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rc->committer_time, TM_LONG)) == -1) goto err;
#line 608 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Message:</dt><dd class=\"commit-msg\">", 45)) == -1) goto err;
#line 608 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 611 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Actions:</dt><dd><a href=\"", 35)) == -1) goto err;
#line 611 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &briefs_url)) == -1) goto err;
#line 614 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">History</a>", 13)) == -1) goto err;
#line 614 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 615 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &blame_url)) == -1) goto err;
#line 618 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Blame</a>", 11)) == -1) goto err;
#line 618 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 619 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &raw_url)) == -1) goto err;
#line 628 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Raw File</a></dd></dl></div><hr /><div id=\"blob\"><pre>", 56)) == -1) goto err;
#line 628 "../gotwebd/pages.tmpl"
if ((tp_ret = got_output_blob_by_lines(tp, blob, gotweb_render_blob_line)) == -1) goto err;
#line 632 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pre></div></div>", 18)) == -1) goto err;
err:
return tp_ret;
}
#line 635 "../gotwebd/pages.tmpl"
int
gotweb_render_blob_line(struct template *tp, const char *line, size_t no)
{
int tp_ret = 0;
#line 637 "../gotwebd/pages.tmpl"
char lineno[16];
int r;
#line 640 "../gotwebd/pages.tmpl"
r = snprintf(lineno, sizeof(lineno), "%zu", no);
if (r < 0 || (size_t)r >= sizeof(lineno))
return -1;
#line 644 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"blob_line\" id=\"line", 31)) == -1) goto err;
#line 644 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, lineno)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "\"><a href=\"#line", 16)) == -1) goto err;
#line 645 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, lineno)) == -1)
goto err;
#line 645 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 645 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, lineno)) == -1)
goto err;
#line 645 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</a><span class=\"blob_code\">", 28)) == -1) goto err;
#line 646 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, line)) == -1)
goto err;
#line 648 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span></div>", 13)) == -1) goto err;
err:
return tp_ret;
}
#line 650 "../gotwebd/pages.tmpl"
int
tree_listing(struct template *tp)
{
int tp_ret = 0;
#line 652 "../gotwebd/pages.tmpl"
const struct got_error *error;
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = c->t->qs;
struct gotweb_url url;
char *readme = NULL;
int binary;
const uint8_t *buf;
size_t len;
#line 663 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<table id=\"tree\">", 17)) == -1) goto err;
#line 663 "../gotwebd/pages.tmpl"
if ((tp_ret = got_output_repo_tree(c, &readme, gotweb_render_tree_item)) == -1) goto err;
#line 665 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</table>", 8)) == -1) goto err;
#line 665 "../gotwebd/pages.tmpl"
if (readme) {
#line 667 "../gotwebd/pages.tmpl"
error = got_open_blob_for_output(&t->blob, &t->fd, &binary, c,
qs->folder, readme, qs->commit);
if (error) {
free(readme);
return (-1);
}
#line 674 "../gotwebd/pages.tmpl"
memset(&url, 0, sizeof(url));
url.index_page = -1;
url.action = BLOB;
url.path = t->qs->path;
url.file = readme;
url.folder = t->qs->folder ? t->qs->folder : "";
url.commit = t->qs->commit;
#line 682 "../gotwebd/pages.tmpl"
if (!binary) {
#line 684 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<h2><a href=\"", 13)) == -1) goto err;
#line 684 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 685 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, readme)) == -1)
goto err;
#line 689 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></h2><pre>", 14)) == -1) goto err;
for (;;) {
error = got_object_blob_read_block(&len, t->blob);
if (error) {
free(readme);
return (-1);
}
if (len == 0)
break;
buf = got_object_blob_get_read_buf(t->blob);
if (tp_write_htmlescape(tp, buf, len) == -1) {
free(readme);
return (-1);
}
}
#line 706 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pre>", 6)) == -1) goto err;
}
}
#line 708 "../gotwebd/pages.tmpl"
err:
free(readme); 
return tp_ret;
}
#line 712 "../gotwebd/pages.tmpl"
int
gotweb_render_tree(struct template *tp)
{
int tp_ret = 0;
#line 714 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
#line 725 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class='subtitle'><h2>Tree</h2></header><div id=\"tree_content\"><div class=\"page_header_wrapper\"><dl><dt>Tree:</dt><dd><code class=\"commit-id\">", 149)) == -1) goto err;
#line 725 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->tree_id)) == -1)
goto err;
#line 728 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</code></dd><dt>Date:</dt><dd>", 30)) == -1) goto err;
#line 728 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rc->committer_time, TM_LONG)) == -1) goto err;
#line 731 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Message:</dt><dd class=\"commit-msg\">", 45)) == -1) goto err;
#line 731 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 735 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd></dl></div><hr />", 22)) == -1) goto err;
#line 735 "../gotwebd/pages.tmpl"
if ((tp_ret = tree_listing(tp)) == -1) goto err;
#line 737 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 740 "../gotwebd/pages.tmpl"
int
gotweb_render_tree_item(struct template *tp, struct got_tree_entry *te)
{
int tp_ret = 0;
#line 742 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
const char *modestr = "";
const char *name;
const char *folder;
char *dir = NULL;
mode_t mode;
struct gotweb_url url = {
.index_page = -1,
.commit = rc->commit_id,
.path = qs->path,
};
#line 757 "../gotwebd/pages.tmpl"
name = got_tree_entry_get_name(te);
mode = got_tree_entry_get_mode(te);
#line 760 "../gotwebd/pages.tmpl"
folder = qs->folder ? qs->folder : "";
if (S_ISDIR(mode)) {
if (asprintf(&dir, "%s/%s", folder, name) == -1)
return (-1);
#line 765 "../gotwebd/pages.tmpl"
url.action = TREE;
url.folder = dir;
} else {
url.action = BLOB;
url.folder = folder;
url.file = name;
}
#line 773 "../gotwebd/pages.tmpl"
if (got_object_tree_entry_is_submodule(te))
modestr = "$";
else if (S_ISLNK(mode))
modestr = "@";
else if (S_ISDIR(mode))
modestr = "/";
else if (mode & S_IXUSR)
modestr = "*";
#line 783 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<tr class=\"tree_wrapper\">", 25)) == -1) goto err;
#line 783 "../gotwebd/pages.tmpl"
if (S_ISDIR(mode)) {
#line 785 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<td class=\"tree_line\" colspan=2><a href=\"", 41)) == -1) goto err;
#line 785 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 786 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, name)) == -1)
goto err;
#line 786 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, modestr)) == -1)
goto err;
#line 789 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></td>", 9)) == -1) goto err;
#line 789 "../gotwebd/pages.tmpl"
} else {
#line 791 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<td class=\"tree_line\"><a href=\"", 31)) == -1) goto err;
#line 791 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 792 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, name)) == -1)
goto err;
#line 792 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, modestr)) == -1)
goto err;
#line 796 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></td><td class=\"tree_line_blank\">", 37)) == -1) goto err;
#line 796 "../gotwebd/pages.tmpl"
url.action = COMMITS; 
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 797 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 800 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">commits</a>", 13)) == -1) goto err;
#line 800 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
url.action = BLAME; 
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 802 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 806 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">blame</a></td>", 16)) == -1) goto err;
}
#line 808 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</tr>", 5)) == -1) goto err;
#line 808 "../gotwebd/pages.tmpl"
err:
#line 810 "../gotwebd/pages.tmpl"
free(dir);
return tp_ret;
}
#line 814 "../gotwebd/pages.tmpl"
int
gotweb_render_tags(struct template *tp)
{
int tp_ret = 0;
#line 816 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_tag *rt;
#line 824 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class='subtitle'><h2>Tags</h2></header><div id=\"tags_content\">", 70)) == -1) goto err;
#line 824 "../gotwebd/pages.tmpl"
if (TAILQ_EMPTY(&t->repo_tags)) {
#line 828 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div id=\"err_content\">This repository contains no tags</div>", 60)) == -1) goto err;
#line 828 "../gotwebd/pages.tmpl"
} else {
TAILQ_FOREACH(rt, &t->repo_tags, entry) {
#line 830 "../gotwebd/pages.tmpl"
if ((tp_ret = tag_item(tp, rt)) == -1) goto err;
}
#line 832 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_more(tp, TAGS)) == -1) goto err;
}
#line 835 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 837 "../gotwebd/pages.tmpl"
int
tag_item(struct template *tp, struct repo_tag *rt)
{
int tp_ret = 0;
#line 839 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
char *tag_name = rt->tag_name;
char *msg = rt->tag_commit;
char *nl;
struct gotweb_url url = {
.action = TAG,
.index_page = -1,
.path = repo_dir->name,
.commit = rt->commit_id,
};
#line 852 "../gotwebd/pages.tmpl"
if (strncmp(tag_name, "refs/tags/", 10) == 0)
tag_name += 10;
#line 855 "../gotwebd/pages.tmpl"
if (msg) {
nl = strchr(msg, '\n');
if (nl)
*nl = '\0';
}
#line 862 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"tag_age\">", 21)) == -1) goto err;
#line 862 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rt->tagger_time, TM_DIFF)) == -1) goto err;
#line 864 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"tag_name\">", 28)) == -1) goto err;
#line 864 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, tag_name)) == -1)
goto err;
#line 866 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"tag_log\"><a href=\"", 36)) == -1) goto err;
#line 866 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 867 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, msg)) == -1)
goto err;
#line 872 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></div><div class=\"navs_wrapper\"><div class=\"navs\"><a href=\"", 63)) == -1) goto err;
#line 872 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">tag</a>", 9)) == -1) goto err;
#line 873 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
url.action = BRIEFS; 
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 875 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">commit briefs</a>", 19)) == -1) goto err;
#line 876 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
url.action = COMMITS; 
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 878 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 882 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">commits</a></div></div><hr />", 31)) == -1) goto err;
err:
return tp_ret;
}
#line 884 "../gotwebd/pages.tmpl"
int
gotweb_render_tag(struct template *tp)
{
int tp_ret = 0;
#line 886 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_tag *rt;
const char *tag_name;
#line 891 "../gotwebd/pages.tmpl"
rt = TAILQ_LAST(&t->repo_tags, repo_tags_head);
tag_name = rt->tag_name;
#line 894 "../gotwebd/pages.tmpl"
if (strncmp(tag_name, "refs/", 5) == 0)
tag_name += 5;
#line 905 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class=\"subtitle\"><h2>Tag</h2></header><div id=\"tags_content\"><div class=\"page_header_wrapper\"><dl><dt>Commit:</dt><dd><code class=\"commit-id\">", 150)) == -1) goto err;
#line 905 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rt->commit_id)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</code>", 7)) == -1) goto err;
#line 906 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<span class=\"refs_str\">(", 24)) == -1) goto err;
#line 907 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, tag_name)) == -1)
goto err;
#line 910 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, ")</span></dd><dt>Tagger:</dt><dd>", 33)) == -1) goto err;
#line 910 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rt->tagger)) == -1)
goto err;
#line 913 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Date:</dt><dd>", 23)) == -1) goto err;
#line 913 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rt->tagger_time, TM_LONG)) == -1) goto err;
#line 916 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Message:</dt><dd class=\"commit-msg\">", 45)) == -1) goto err;
#line 916 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rt->commit_msg)) == -1)
goto err;
#line 920 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd></dl><hr /><pre id=\"tag_commit\">", 37)) == -1) goto err;
#line 920 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rt->tag_commit)) == -1)
goto err;
#line 924 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pre></div></div>", 18)) == -1) goto err;
err:
return tp_ret;
}
#line 926 "../gotwebd/pages.tmpl"
int
gotweb_render_diff(struct template *tp)
{
int tp_ret = 0;
#line 928 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
FILE *fp = t->fp;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
char *line = NULL;
size_t linesize = 0;
ssize_t linelen;
struct gotweb_url patch_url, tree_url = {
.action = TREE,
.index_page = -1,
.path = qs->path,
.commit = rc->commit_id,
};
#line 943 "../gotwebd/pages.tmpl"
memcpy(&patch_url, &tree_url, sizeof(patch_url));
patch_url.action = PATCH;
#line 953 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class=\"subtitle\"><h2>Commit Diff</h2></header><div id=\"diff_content\"><div class=\"page_header_wrapper\"><dl><dt>Commit:</dt><dd><code class=\"commit-id\">", 158)) == -1) goto err;
#line 953 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_id)) == -1)
goto err;
#line 955 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</code></dd><dt>From:</dt><dd>", 30)) == -1) goto err;
#line 955 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->author)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
#line 956 "../gotwebd/pages.tmpl"
if (strcmp(rc->committer, rc->author) != 0) {
#line 958 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Via:</dt><dd>", 17)) == -1) goto err;
#line 958 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->committer)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
}
#line 962 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Date:</dt><dd>", 18)) == -1) goto err;
#line 962 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rc->committer_time, TM_LONG)) == -1) goto err;
#line 965 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Message:</dt><dd class=\"commit-msg\">", 45)) == -1) goto err;
#line 965 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 968 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Actions:</dt><dd><a href=\"", 35)) == -1) goto err;
#line 968 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &patch_url)) == -1) goto err;
#line 971 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Patch</a>", 11)) == -1) goto err;
#line 971 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 972 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &tree_url)) == -1) goto err;
#line 980 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Tree</a></dd></dl></div><hr /><pre id=\"diff\">", 47)) == -1) goto err;
while ((linelen = getline(&line, &linesize, fp)) != -1) {
if ((tp_ret = diff_line(tp, line)) == -1) goto err;
}
#line 985 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pre></div>", 12)) == -1) goto err;
#line 985 "../gotwebd/pages.tmpl"
err:
free(line); 
return tp_ret;
}
#line 989 "../gotwebd/pages.tmpl"
int
diff_line(struct template *tp, char *line )
{
int tp_ret = 0;
#line 991 "../gotwebd/pages.tmpl"
const char *color = NULL;
char *nl;
#line 994 "../gotwebd/pages.tmpl"
if (!strncmp(line, "-", 1))
color = "diff_minus";
else if (!strncmp(line, "+", 1))
color = "diff_plus";
else if (!strncmp(line, "@@", 2))
color = "diff_chunk_header";
else if (!strncmp(line, "commit +", 8) ||
!strncmp(line, "commit -", 8) ||
!strncmp(line, "blob +", 6) ||
!strncmp(line, "blob -", 6) ||
!strncmp(line, "file +", 6) ||
!strncmp(line, "file -", 6))
color = "diff_meta";
else if (!strncmp(line, "from:", 5) || !strncmp(line, "via:", 4))
color = "diff_author";
else if (!strncmp(line, "date:", 5))
color = "diff_date";
#line 1012 "../gotwebd/pages.tmpl"
nl = strchr(line, '\n');
if (nl)
*nl = '\0';
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<span class=\"diff_line ", 23)) == -1) goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, color)) == -1)
goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, line)) == -1)
goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span>", 7)) == -1) goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
err:
return tp_ret;
}
#line 1020 "../gotwebd/pages.tmpl"
int
gotweb_render_branches(struct template *tp, struct got_reflist_head *refs)
{
int tp_ret = 0;
#line 1022 "../gotwebd/pages.tmpl"
struct got_reflist_entry *re;
#line 1028 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class='subtitle'><h2>Branches</h2></header><div id=\"branches_content\">", 78)) == -1) goto err;
TAILQ_FOREACH(re, refs, entry) {
if (!got_ref_is_symbolic(re->ref)) {
if ((tp_ret = branch(tp, re)) == -1) goto err;
}
}
#line 1034 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div>", 6)) == -1) goto err;
err:
return tp_ret;
}
#line 1036 "../gotwebd/pages.tmpl"
int
branch(struct template *tp, struct got_reflist_entry *re)
{
int tp_ret = 0;
#line 1038 "../gotwebd/pages.tmpl"
const struct got_error *err;
struct request *c = tp->tp_arg;
struct querystring *qs = c->t->qs;
const char *refname;
time_t age;
struct gotweb_url url = {
.action = SUMMARY,
.index_page = -1,
.path = qs->path,
};
#line 1049 "../gotwebd/pages.tmpl"
refname = got_ref_get_name(re->ref);
#line 1051 "../gotwebd/pages.tmpl"
err = got_get_repo_age(&age, c, refname);
if (err) {
log_warnx("%s: %s", __func__, err->msg);
return -1;
}
#line 1057 "../gotwebd/pages.tmpl"
if (strncmp(refname, "refs/heads/", 11) == 0)
refname += 11;
#line 1060 "../gotwebd/pages.tmpl"
url.headref = refname;
#line 1064 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<section class=\"branches_wrapper\"><div class=\"branches_age\">", 60)) == -1) goto err;
#line 1064 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, age, TM_DIFF)) == -1) goto err;
#line 1067 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"branch\"><a href=\"", 35)) == -1) goto err;
#line 1067 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 1067 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 1067 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, refname)) == -1)
goto err;
#line 1071 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></div><div class=\"navs_wrapper\"><div class=\"navs\"><a href=\"", 63)) == -1) goto err;
#line 1071 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">summary</a>", 13)) == -1) goto err;
#line 1072 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
url.action = BRIEFS; 
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 1074 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">commit briefs</a>", 19)) == -1) goto err;
#line 1075 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
url.action = COMMITS; 
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 1077 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 1082 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">commits</a></div></div><hr /></section>", 41)) == -1) goto err;
err:
return tp_ret;
}
#line 1084 "../gotwebd/pages.tmpl"
int
gotweb_render_summary(struct template *tp)
{
int tp_ret = 0;
#line 1086 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct transport *t = c->t;
struct got_reflist_head *refs = &t->refs;
#line 1092 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dl id=\"summary_wrapper\" class=\"page_header_wrapper\">", 53)) == -1) goto err;
#line 1092 "../gotwebd/pages.tmpl"
if (srv->show_repo_description) {
#line 1094 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Description:</dt><dd>", 25)) == -1) goto err;
#line 1094 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, t->repo_dir->description)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
}
if (srv->show_repo_owner) {
#line 1098 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Owner:</dt><dd>", 19)) == -1) goto err;
#line 1098 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, t->repo_dir->owner)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
}
if (srv->show_repo_age) {
#line 1103 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Last Change:</dt><dd>", 25)) == -1) goto err;
#line 1103 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, t->repo_dir->age, TM_DIFF)) == -1) goto err;
#line 1105 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd>", 5)) == -1) goto err;
}
if (srv->show_repo_cloneurl) {
#line 1108 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<dt>Clone URL:</dt><dd><pre class=\"clone-url\">", 46)) == -1) goto err;
#line 1108 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, t->repo_dir->url)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</pre></dd>", 11)) == -1) goto err;
}
#line 1112 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dl><div class=\"summary-briefs\">", 33)) == -1) goto err;
#line 1112 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_briefs(tp)) == -1) goto err;
#line 1115 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"summary-branches\">", 36)) == -1) goto err;
#line 1115 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_branches(tp, refs)) == -1) goto err;
#line 1118 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"summary-tags\">", 32)) == -1) goto err;
#line 1118 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_tags(tp)) == -1) goto err;
#line 1125 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div><div class=\"summary-tree\"><header class='subtitle'><h2>Tree</h2></header><div id=\"tree_content\">", 102)) == -1) goto err;
#line 1125 "../gotwebd/pages.tmpl"
if ((tp_ret = tree_listing(tp)) == -1) goto err;
#line 1128 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</div></div>", 12)) == -1) goto err;
err:
return tp_ret;
}
#line 1130 "../gotwebd/pages.tmpl"
int
gotweb_render_blame(struct template *tp)
{
int tp_ret = 0;
#line 1132 "../gotwebd/pages.tmpl"
const struct got_error *err;
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
struct gotweb_url briefs_url, blob_url, raw_url;
#line 1139 "../gotwebd/pages.tmpl"
memset(&briefs_url, 0, sizeof(briefs_url));
briefs_url.index_page = -1,
briefs_url.action = BRIEFS,
briefs_url.path = qs->path,
briefs_url.commit = qs->commit,
briefs_url.folder = qs->folder,
briefs_url.file = qs->file,
#line 1147 "../gotwebd/pages.tmpl"
memcpy(&blob_url, &briefs_url, sizeof(blob_url));
blob_url.action = BLOB;
#line 1150 "../gotwebd/pages.tmpl"
memcpy(&raw_url, &briefs_url, sizeof(raw_url));
raw_url.action = BLOBRAW;
#line 1161 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<header class=\"subtitle\"><h2>Blame</h2></header><div id=\"blame_content\"><div class=\"page_header_wrapper\"><dl><dt>Date:</dt><dd>", 127)) == -1) goto err;
#line 1161 "../gotwebd/pages.tmpl"
if ((tp_ret = datetime(tp, rc->committer_time, TM_LONG)) == -1) goto err;
#line 1164 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Message:</dt><dd class=\"commit-msg\">", 45)) == -1) goto err;
#line 1164 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_msg)) == -1)
goto err;
#line 1167 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</dd><dt>Actions:</dt><dd><a href=\"", 35)) == -1) goto err;
#line 1167 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &briefs_url)) == -1) goto err;
#line 1170 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">History</a>", 13)) == -1) goto err;
#line 1170 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 1171 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &blob_url)) == -1) goto err;
#line 1174 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Blob</a>", 10)) == -1) goto err;
#line 1174 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<a href=\"", 9)) == -1) goto err;
#line 1175 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &raw_url)) == -1) goto err;
#line 1183 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "\">Raw File</a></dd></dl></div><hr /><pre id=\"blame\">", 52)) == -1) goto err;
err = got_output_file_blame(c, &blame_line);
if (err && err->code != GOT_ERR_CANCELLED)
log_warnx("%s: got_output_file_blame: %s", __func__,
err->msg);
if (err)
return (-1);
#line 1193 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pre></div>", 12)) == -1) goto err;
err:
return tp_ret;
}
#line 1196 "../gotwebd/pages.tmpl"
int
blame_line(struct template *tp, const char *line, struct blame_line *bline, int lprec, int lcur)
{
int tp_ret = 0;
#line 1198 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
char *committer, *s;
struct gotweb_url url = {
.action = DIFF,
.index_page = -1,
.path = repo_dir->name,
.commit = bline->id_str,
};
#line 1209 "../gotwebd/pages.tmpl"
s = strchr(bline->committer, '<');
committer = s ? s + 1 : bline->committer;
#line 1212 "../gotwebd/pages.tmpl"
s = strchr(committer, '@');
if (s)
*s = '\0';
#line 1217 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<div class=\"blame_line\"><span class=\"blame_number\">", 51)) == -1) goto err;
#line 1217 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%*d ", lprec, lcur) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
#line 1219 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span><span class=\"blame_hash\"><a href=\"", 41)) == -1) goto err;
#line 1219 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
if ((tp_ret = tp_write(tp, "\">", 2)) == -1) goto err;
#line 1220 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%.8s", bline->id_str) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
#line 1223 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</a></span>", 11)) == -1) goto err;
#line 1223 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<span class=\"blame_date\">", 25)) == -1) goto err;
#line 1224 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, bline->datebuf)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</span>", 7)) == -1) goto err;
#line 1225 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<span class=\"blame_author\">", 27)) == -1) goto err;
#line 1226 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%.9s", committer) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
if ((tp_ret = tp_write(tp, "</span>", 7)) == -1) goto err;
#line 1227 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "<span class=\"blame_code\">", 25)) == -1) goto err;
#line 1228 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, line)) == -1)
goto err;
#line 1230 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</span></div>", 13)) == -1) goto err;
err:
return tp_ret;
}
#line 1232 "../gotwebd/pages.tmpl"
int
gotweb_render_patch(struct template *tp)
{
int tp_ret = 0;
#line 1234 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
struct tm tm;
char buf[BUFSIZ], datebuf[64];
size_t r;
#line 1241 "../gotwebd/pages.tmpl"
if (gmtime_r(&rc->committer_time, &tm) == NULL ||
strftime(datebuf, sizeof(datebuf), "%a %b %d %T %Y UTC", &tm) == 0)
return (-1);
#line 1245 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "commit ", 7)) == -1) goto err;
#line 1245 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rc->commit_id)) == -1)
goto err;
#line 1245 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp_write(tp, "from: ", 6)) == -1) goto err;
#line 1246 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_writes(tp, rc->author)) == -1)
goto err;
#line 1246 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
if (strcmp(rc->committer, rc->author) != 0) {
if ((tp_ret = tp_write(tp, "via: ", 5)) == -1) goto err;
#line 1248 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_writes(tp, rc->committer)) == -1)
goto err;
#line 1248 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
}
#line 1250 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "date: ", 6)) == -1) goto err;
#line 1250 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, datebuf)) == -1)
goto err;
#line 1250 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp_writes(tp, rc->commit_msg)) == -1)
goto err;
#line 1252 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, "\n")) == -1)
goto err;
#line 1254 "../gotwebd/pages.tmpl"
if (template_flush(tp) == -1)
return (-1);
for (;;) {
r = fread(buf, 1, sizeof(buf), t->fp);
if (fcgi_write(c, buf, r) == -1 ||
r != sizeof(buf))
break;
}
err:
return tp_ret;
}
#line 1265 "../gotwebd/pages.tmpl"
int
gotweb_render_rss(struct template *tp)
{
int tp_ret = 0;
#line 1267 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
struct repo_tag *rt;
struct gotweb_url summary = {
.action = SUMMARY,
.index_page = -1,
.path = repo_dir->name,
};
#line 1281 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><rss version=\"2.0\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\"><channel><title>Tags of ", 138)) == -1) goto err;
#line 1281 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, repo_dir->name)) == -1)
goto err;
#line 1284 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</title><link><![CDATA[", 23)) == -1) goto err;
#line 1284 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_absolute_url(c, &summary)) == -1) goto err;
#line 1287 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "]]></link>", 10)) == -1) goto err;
#line 1287 "../gotwebd/pages.tmpl"
if (srv->show_repo_description) {
if ((tp_ret = tp_write(tp, "<description>", 13)) == -1) goto err;
#line 1288 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, repo_dir->description)) == -1)
goto err;
if ((tp_ret = tp_write(tp, "</description>", 14)) == -1) goto err;
}
TAILQ_FOREACH(rt, &t->repo_tags, entry) {
#line 1291 "../gotwebd/pages.tmpl"
if ((tp_ret = rss_tag_item(tp, rt)) == -1) goto err;
}
#line 1295 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</channel></rss>", 16)) == -1) goto err;
err:
return tp_ret;
}
#line 1297 "../gotwebd/pages.tmpl"
int
rss_tag_item(struct template *tp, struct repo_tag *rt)
{
int tp_ret = 0;
#line 1299 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
struct tm tm;
char rfc822[128];
int r;
char *tag_name = rt->tag_name;
struct gotweb_url tag = {
.action = TAG,
.index_page = -1,
.path = repo_dir->name,
.commit = rt->commit_id,
};
#line 1313 "../gotwebd/pages.tmpl"
if (strncmp(tag_name, "refs/tags/", 10) == 0)
tag_name += 10;
#line 1316 "../gotwebd/pages.tmpl"
if (gmtime_r(&rt->tagger_time, &tm) == NULL)
return -1;
r = strftime(rfc822, sizeof(rfc822), "%a, %d %b %Y %H:%M:%S GMT", &tm);
if (r == 0)
return 0;
#line 1323 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<item><title>", 13)) == -1) goto err;
#line 1323 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, repo_dir->name)) == -1)
goto err;
#line 1323 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 1323 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, tag_name)) == -1)
goto err;
#line 1326 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</title><link><![CDATA[", 23)) == -1) goto err;
#line 1326 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_absolute_url(c, &tag)) == -1) goto err;
#line 1330 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "]]></link><description><![CDATA[<pre>", 37)) == -1) goto err;
#line 1330 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rt->tag_commit)) == -1)
goto err;
#line 1332 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pre>]]></description>", 23)) == -1) goto err;
#line 1332 "../gotwebd/pages.tmpl"
if ((tp_ret = rss_author(tp, rt->tagger)) == -1) goto err;
if ((tp_ret = tp_write(tp, "<guid isPermaLink=\"false\">", 26)) == -1) goto err;
#line 1333 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rt->commit_id)) == -1)
goto err;
#line 1335 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</guid><pubDate>", 16)) == -1) goto err;
#line 1335 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, rfc822)) == -1)
goto err;
#line 1338 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "</pubDate></item>", 17)) == -1) goto err;
err:
return tp_ret;
}
#line 1340 "../gotwebd/pages.tmpl"
int
rss_author(struct template *tp, char *author)
{
int tp_ret = 0;
#line 1342 "../gotwebd/pages.tmpl"
char *t, *mail;
#line 1344 "../gotwebd/pages.tmpl"
/* what to do if the author name contains a paren? */
if (strchr(author, '(') != NULL || strchr(author, ')') != NULL)
return 0;
#line 1348 "../gotwebd/pages.tmpl"
t = strchr(author, '<');
if (t == NULL)
return 0;
*t = '\0';
mail = t+1;
#line 1354 "../gotwebd/pages.tmpl"
while (isspace((unsigned char)*--t))
*t = '\0';
#line 1357 "../gotwebd/pages.tmpl"
t = strchr(mail, '>');
if (t == NULL)
return 0;
*t = '\0';
#line 1363 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "<author>", 8)) == -1) goto err;
#line 1363 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, mail)) == -1)
goto err;
#line 1363 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, " ")) == -1)
goto err;
#line 1363 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, "(", 1)) == -1) goto err;
#line 1363 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_htmlescape(tp, author)) == -1)
goto err;
#line 1365 "../gotwebd/pages.tmpl"
if ((tp_ret = tp_write(tp, ")</author>", 10)) == -1) goto err;
err:
return tp_ret;
}
